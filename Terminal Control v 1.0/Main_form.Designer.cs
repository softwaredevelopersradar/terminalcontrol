﻿namespace Terminal_Control_v_1._1
{
    partial class Main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_form));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_err = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox_params = new System.Windows.Forms.GroupBox();
            this.comboBox_litera_FPS1 = new System.Windows.Forms.ComboBox();
            this.cb_tim4_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_tim3_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_tim2_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_man4_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_dev4_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_mod4_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_man3_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_dev3_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_mod3_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_man2_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_dev2_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_mod2_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_man1_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_dev1_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_mod1_fps1 = new System.Windows.Forms.ComboBox();
            this.cb_tim1_fps1 = new System.Windows.Forms.ComboBox();
            this.comboBox_freq_num_fps1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_duration_code = new System.Windows.Forms.Label();
            this.tb_freq4_fps1 = new System.Windows.Forms.TextBox();
            this.label_code_freq = new System.Windows.Forms.Label();
            this.tb_freq3_fps1 = new System.Windows.Forms.TextBox();
            this.tb_freq2_fps1 = new System.Windows.Forms.TextBox();
            this.label_freq_num = new System.Windows.Forms.Label();
            this.tb_freq1_fps1 = new System.Windows.Forms.TextBox();
            this.label_nomer_pod = new System.Windows.Forms.Label();
            this.label_band_code = new System.Windows.Forms.Label();
            this.button_power_off_FPS1 = new System.Windows.Forms.Button();
            this.button_param_FPS1 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listBoxErr = new System.Windows.Forms.ListBox();
            this.label24 = new System.Windows.Forms.Label();
            this.labelGetNv = new System.Windows.Forms.Label();
            this.labelGetNn = new System.Windows.Forms.Label();
            this.labeVer = new System.Windows.Forms.Label();
            this.button_Get_Ver_FPS2 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labelParamK = new System.Windows.Forms.Label();
            this.button_GetAmp_FPS2 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_ampN_fps2 = new System.Windows.Forms.TextBox();
            this.button_power2_off_FPS2 = new System.Windows.Forms.Button();
            this.button_Test_FPS2 = new System.Windows.Forms.Button();
            this.tb_ampV_fps2 = new System.Windows.Forms.TextBox();
            this.button_Amp_FPS2 = new System.Windows.Forms.Button();
            this.button_power_off_all_FPS2 = new System.Windows.Forms.Button();
            this.button_status_on_FPS2 = new System.Windows.Forms.Button();
            this.button_power_on_FPS2 = new System.Windows.Forms.Button();
            this.tb_dev2_fps2 = new System.Windows.Forms.TextBox();
            this.checkBox_GPS_L2 = new System.Windows.Forms.CheckBox();
            this.checkBox_GPS_L1 = new System.Windows.Forms.CheckBox();
            this.checkBox_GLONASS_L2 = new System.Windows.Forms.CheckBox();
            this.checkBox_GLONASS_L1 = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_dev1_fps2 = new System.Windows.Forms.TextBox();
            this.tb_man2_fps2 = new System.Windows.Forms.TextBox();
            this.tb_man1_fps2 = new System.Windows.Forms.TextBox();
            this.cb_tim2_fps2 = new System.Windows.Forms.ComboBox();
            this.cb_man2_fps2 = new System.Windows.Forms.ComboBox();
            this.cb_mod2_fps2 = new System.Windows.Forms.ComboBox();
            this.cb_man1_fps2 = new System.Windows.Forms.ComboBox();
            this.cb_mod1_fps2 = new System.Windows.Forms.ComboBox();
            this.cb_tim1_fps2 = new System.Windows.Forms.ComboBox();
            this.comboBox_freq_num_fps2 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_freq2_fps2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_freq1_fps2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button_param_FPS2 = new System.Windows.Forms.Button();
            this.button_power_off_FPS2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_litera_FPS2 = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.saveParamAtBooster = new System.Windows.Forms.Button();
            this.getParams = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label47 = new System.Windows.Forms.Label();
            this.cb_literaCH_fps3 = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.listBoxErr_fps3 = new System.Windows.Forms.ListBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.bt_Get_Ver_FPS2 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.bt_GetAmp_fps3 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.tb_ampNn_fps3 = new System.Windows.Forms.TextBox();
            this.tb_ampNv_fps3 = new System.Windows.Forms.TextBox();
            this.bt_Amp_fps3 = new System.Windows.Forms.Button();
            this.bt_get_status_fps3 = new System.Windows.Forms.Button();
            this.checkBox_GPS_L2_fps3 = new System.Windows.Forms.CheckBox();
            this.checkBox_GPS_L1_fps3 = new System.Windows.Forms.CheckBox();
            this.checkBox_GL_L2_fps3 = new System.Windows.Forms.CheckBox();
            this.checkBox_GL_L1_fps3 = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_dev1_fps3 = new System.Windows.Forms.TextBox();
            this.tb_man1_fps3 = new System.Windows.Forms.TextBox();
            this.cb_man1_fps3 = new System.Windows.Forms.ComboBox();
            this.cb_mod1_fps3 = new System.Windows.Forms.ComboBox();
            this.cb_tim1_fps3 = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tb_freq2_fps3 = new System.Windows.Forms.TextBox();
            this.tb_freq1_fps3 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.bt_set_param_FPS3 = new System.Windows.Forms.Button();
            this.bt_power_off_All_FPS3 = new System.Windows.Forms.Button();
            this.bt_power_off_FPS3 = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.cb_literaK_fps3 = new System.Windows.Forms.ComboBox();
            this.cb_litera_fps3 = new System.Windows.Forms.ComboBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxStopSend = new System.Windows.Forms.CheckBox();
            this.checkBox_test_HEX = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.bt_open_hex = new System.Windows.Forms.Button();
            this.checkBox_color = new System.Windows.Forms.CheckBox();
            this.textBox_buf_delay = new System.Windows.Forms.TextBox();
            this.checkBox_visibleTxRx = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button_buffer_delay = new System.Windows.Forms.Button();
            this.button_update_ports = new System.Windows.Forms.Button();
            this.comboBox_portname = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.button_Open = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox_baudrate = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_parity = new System.Windows.Forms.ComboBox();
            this.comboBox_stop_bits = new System.Windows.Forms.ComboBox();
            this.checkBoxService = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label15 = new System.Windows.Forms.Label();
            this.richTextBoxOut = new System.Windows.Forms.RichTextBox();
            this.button_terminal = new System.Windows.Forms.Button();
            this.button_terminal_clear = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.openFileDialogHex = new System.Windows.Forms.OpenFileDialog();
            this.timerWaitStOK = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox_params.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel_err});
            this.statusStrip1.Location = new System.Drawing.Point(0, 621);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1069, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(103, 17);
            this.toolStripStatusLabel1.Text = "Port not initialized";
            // 
            // toolStripStatusLabel_err
            // 
            this.toolStripStatusLabel_err.Name = "toolStripStatusLabel_err";
            this.toolStripStatusLabel_err.Size = new System.Drawing.Size(0, 17);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 72);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1069, 413);
            this.tabControl1.TabIndex = 50;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox_params);
            this.tabPage1.Controls.Add(this.button_power_off_FPS1);
            this.tabPage1.Controls.Add(this.button_param_FPS1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1061, 387);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Управление ФПС1";
            // 
            // groupBox_params
            // 
            this.groupBox_params.Controls.Add(this.comboBox_litera_FPS1);
            this.groupBox_params.Controls.Add(this.cb_tim4_fps1);
            this.groupBox_params.Controls.Add(this.cb_tim3_fps1);
            this.groupBox_params.Controls.Add(this.cb_tim2_fps1);
            this.groupBox_params.Controls.Add(this.cb_man4_fps1);
            this.groupBox_params.Controls.Add(this.cb_dev4_fps1);
            this.groupBox_params.Controls.Add(this.cb_mod4_fps1);
            this.groupBox_params.Controls.Add(this.cb_man3_fps1);
            this.groupBox_params.Controls.Add(this.cb_dev3_fps1);
            this.groupBox_params.Controls.Add(this.cb_mod3_fps1);
            this.groupBox_params.Controls.Add(this.cb_man2_fps1);
            this.groupBox_params.Controls.Add(this.cb_dev2_fps1);
            this.groupBox_params.Controls.Add(this.cb_mod2_fps1);
            this.groupBox_params.Controls.Add(this.cb_man1_fps1);
            this.groupBox_params.Controls.Add(this.cb_dev1_fps1);
            this.groupBox_params.Controls.Add(this.cb_mod1_fps1);
            this.groupBox_params.Controls.Add(this.cb_tim1_fps1);
            this.groupBox_params.Controls.Add(this.comboBox_freq_num_fps1);
            this.groupBox_params.Controls.Add(this.label3);
            this.groupBox_params.Controls.Add(this.label1);
            this.groupBox_params.Controls.Add(this.label_duration_code);
            this.groupBox_params.Controls.Add(this.tb_freq4_fps1);
            this.groupBox_params.Controls.Add(this.label_code_freq);
            this.groupBox_params.Controls.Add(this.tb_freq3_fps1);
            this.groupBox_params.Controls.Add(this.tb_freq2_fps1);
            this.groupBox_params.Controls.Add(this.label_freq_num);
            this.groupBox_params.Controls.Add(this.tb_freq1_fps1);
            this.groupBox_params.Controls.Add(this.label_nomer_pod);
            this.groupBox_params.Controls.Add(this.label_band_code);
            this.groupBox_params.Location = new System.Drawing.Point(12, 9);
            this.groupBox_params.Name = "groupBox_params";
            this.groupBox_params.Size = new System.Drawing.Size(489, 198);
            this.groupBox_params.TabIndex = 11;
            this.groupBox_params.TabStop = false;
            // 
            // comboBox_litera_FPS1
            // 
            this.comboBox_litera_FPS1.FormattingEnabled = true;
            this.comboBox_litera_FPS1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_litera_FPS1.Location = new System.Drawing.Point(151, 13);
            this.comboBox_litera_FPS1.Name = "comboBox_litera_FPS1";
            this.comboBox_litera_FPS1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox_litera_FPS1.Size = new System.Drawing.Size(201, 21);
            this.comboBox_litera_FPS1.TabIndex = 2;
            this.comboBox_litera_FPS1.Text = "1";
            // 
            // cb_tim4_fps1
            // 
            this.cb_tim4_fps1.Enabled = false;
            this.cb_tim4_fps1.FormattingEnabled = true;
            this.cb_tim4_fps1.Items.AddRange(new object[] {
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim4_fps1.Location = new System.Drawing.Point(370, 162);
            this.cb_tim4_fps1.Name = "cb_tim4_fps1";
            this.cb_tim4_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim4_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_tim4_fps1.TabIndex = 2;
            this.cb_tim4_fps1.Text = "0,5 мс";
            // 
            // cb_tim3_fps1
            // 
            this.cb_tim3_fps1.Enabled = false;
            this.cb_tim3_fps1.FormattingEnabled = true;
            this.cb_tim3_fps1.Items.AddRange(new object[] {
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim3_fps1.Location = new System.Drawing.Point(297, 162);
            this.cb_tim3_fps1.Name = "cb_tim3_fps1";
            this.cb_tim3_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim3_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_tim3_fps1.TabIndex = 2;
            this.cb_tim3_fps1.Text = "0,5 мс";
            // 
            // cb_tim2_fps1
            // 
            this.cb_tim2_fps1.Enabled = false;
            this.cb_tim2_fps1.FormattingEnabled = true;
            this.cb_tim2_fps1.Items.AddRange(new object[] {
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim2_fps1.Location = new System.Drawing.Point(222, 162);
            this.cb_tim2_fps1.Name = "cb_tim2_fps1";
            this.cb_tim2_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim2_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_tim2_fps1.TabIndex = 2;
            this.cb_tim2_fps1.Text = "0,5 мс";
            // 
            // cb_man4_fps1
            // 
            this.cb_man4_fps1.Enabled = false;
            this.cb_man4_fps1.FormattingEnabled = true;
            this.cb_man4_fps1.Items.AddRange(new object[] {
            "----"});
            this.cb_man4_fps1.Location = new System.Drawing.Point(370, 137);
            this.cb_man4_fps1.Name = "cb_man4_fps1";
            this.cb_man4_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man4_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_man4_fps1.TabIndex = 2;
            this.cb_man4_fps1.Text = "----";
            // 
            // cb_dev4_fps1
            // 
            this.cb_dev4_fps1.Enabled = false;
            this.cb_dev4_fps1.FormattingEnabled = true;
            this.cb_dev4_fps1.Items.AddRange(new object[] {
            "---",
            "±1,75",
            "± 2,47",
            "± 3,5",
            "± 4,2",
            "± 5,0",
            "± 7,07",
            "± 10,0",
            "± 15,8",
            "± 25,0",
            "± 35,35",
            "± 50,0",
            "± 70,0",
            "± 100,0",
            "± 150,0",
            "± 250,0",
            "± 350,0",
            "± 500,0",
            "± 700,0",
            "± 1000"});
            this.cb_dev4_fps1.Location = new System.Drawing.Point(370, 113);
            this.cb_dev4_fps1.Name = "cb_dev4_fps1";
            this.cb_dev4_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_dev4_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_dev4_fps1.TabIndex = 2;
            this.cb_dev4_fps1.Text = "±1,75";
            // 
            // cb_mod4_fps1
            // 
            this.cb_mod4_fps1.Enabled = false;
            this.cb_mod4_fps1.FormattingEnabled = true;
            this.cb_mod4_fps1.Items.AddRange(new object[] {
            "---",
            "ЧМШ",
            "ЧМ-2",
            "ЧМ-4",
            "ЧМ-8",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "КФМ"});
            this.cb_mod4_fps1.Location = new System.Drawing.Point(370, 89);
            this.cb_mod4_fps1.Name = "cb_mod4_fps1";
            this.cb_mod4_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod4_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_mod4_fps1.TabIndex = 2;
            this.cb_mod4_fps1.Text = "ЧМШ";
            this.cb_mod4_fps1.SelectedIndexChanged += new System.EventHandler(this.cb_mod4_fps1_SelectedIndexChanged);
            // 
            // cb_man3_fps1
            // 
            this.cb_man3_fps1.Enabled = false;
            this.cb_man3_fps1.FormattingEnabled = true;
            this.cb_man3_fps1.Items.AddRange(new object[] {
            "----"});
            this.cb_man3_fps1.Location = new System.Drawing.Point(297, 137);
            this.cb_man3_fps1.Name = "cb_man3_fps1";
            this.cb_man3_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man3_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_man3_fps1.TabIndex = 2;
            this.cb_man3_fps1.Text = "----";
            // 
            // cb_dev3_fps1
            // 
            this.cb_dev3_fps1.Enabled = false;
            this.cb_dev3_fps1.FormattingEnabled = true;
            this.cb_dev3_fps1.Items.AddRange(new object[] {
            "---",
            "±1,75",
            "± 2,47",
            "± 3,5",
            "± 4,2",
            "± 5,0",
            "± 7,07",
            "± 10,0",
            "± 15,8",
            "± 25,0",
            "± 35,35",
            "± 50,0",
            "± 70,0",
            "± 100,0",
            "± 150,0",
            "± 250,0",
            "± 350,0",
            "± 500,0",
            "± 700,0",
            "± 1000"});
            this.cb_dev3_fps1.Location = new System.Drawing.Point(297, 113);
            this.cb_dev3_fps1.Name = "cb_dev3_fps1";
            this.cb_dev3_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_dev3_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_dev3_fps1.TabIndex = 2;
            this.cb_dev3_fps1.Text = "±1,75";
            // 
            // cb_mod3_fps1
            // 
            this.cb_mod3_fps1.Enabled = false;
            this.cb_mod3_fps1.FormattingEnabled = true;
            this.cb_mod3_fps1.Items.AddRange(new object[] {
            "---",
            "ЧМШ",
            "ЧМ-2",
            "ЧМ-4",
            "ЧМ-8",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "КФМ"});
            this.cb_mod3_fps1.Location = new System.Drawing.Point(297, 89);
            this.cb_mod3_fps1.Name = "cb_mod3_fps1";
            this.cb_mod3_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod3_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_mod3_fps1.TabIndex = 2;
            this.cb_mod3_fps1.Text = "ЧМШ";
            this.cb_mod3_fps1.SelectedIndexChanged += new System.EventHandler(this.cb_mod3_fps1_SelectedIndexChanged);
            // 
            // cb_man2_fps1
            // 
            this.cb_man2_fps1.Enabled = false;
            this.cb_man2_fps1.FormattingEnabled = true;
            this.cb_man2_fps1.Items.AddRange(new object[] {
            "----"});
            this.cb_man2_fps1.Location = new System.Drawing.Point(222, 137);
            this.cb_man2_fps1.Name = "cb_man2_fps1";
            this.cb_man2_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man2_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_man2_fps1.TabIndex = 2;
            this.cb_man2_fps1.Text = "----";
            // 
            // cb_dev2_fps1
            // 
            this.cb_dev2_fps1.Enabled = false;
            this.cb_dev2_fps1.FormattingEnabled = true;
            this.cb_dev2_fps1.Items.AddRange(new object[] {
            "---",
            "±1,75",
            "± 2,47",
            "± 3,5",
            "± 4,2",
            "± 5,0",
            "± 7,07",
            "± 10,0",
            "± 15,8",
            "± 25,0",
            "± 35,35",
            "± 50,0",
            "± 70,0",
            "± 100,0",
            "± 150,0",
            "± 250,0",
            "± 350,0",
            "± 500,0",
            "± 700,0",
            "± 1000"});
            this.cb_dev2_fps1.Location = new System.Drawing.Point(222, 113);
            this.cb_dev2_fps1.Name = "cb_dev2_fps1";
            this.cb_dev2_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_dev2_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_dev2_fps1.TabIndex = 2;
            this.cb_dev2_fps1.Text = "±1,75";
            // 
            // cb_mod2_fps1
            // 
            this.cb_mod2_fps1.Enabled = false;
            this.cb_mod2_fps1.FormattingEnabled = true;
            this.cb_mod2_fps1.Items.AddRange(new object[] {
            "---",
            "ЧМШ",
            "ЧМ-2",
            "ЧМ-4",
            "ЧМ-8",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "КФМ"});
            this.cb_mod2_fps1.Location = new System.Drawing.Point(222, 89);
            this.cb_mod2_fps1.Name = "cb_mod2_fps1";
            this.cb_mod2_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod2_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_mod2_fps1.TabIndex = 2;
            this.cb_mod2_fps1.Text = "ЧМШ";
            this.cb_mod2_fps1.SelectedIndexChanged += new System.EventHandler(this.cb_mod2_fps1_SelectedIndexChanged);
            // 
            // cb_man1_fps1
            // 
            this.cb_man1_fps1.FormattingEnabled = true;
            this.cb_man1_fps1.Items.AddRange(new object[] {
            "----"});
            this.cb_man1_fps1.Location = new System.Drawing.Point(149, 137);
            this.cb_man1_fps1.Name = "cb_man1_fps1";
            this.cb_man1_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man1_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_man1_fps1.TabIndex = 2;
            this.cb_man1_fps1.Text = "----";
            // 
            // cb_dev1_fps1
            // 
            this.cb_dev1_fps1.FormattingEnabled = true;
            this.cb_dev1_fps1.Items.AddRange(new object[] {
            "---",
            "±1,75",
            "± 2,47",
            "± 3,5",
            "± 4,2",
            "± 5,0",
            "± 7,07",
            "± 10,0",
            "± 15,8",
            "± 25,0",
            "± 35,35",
            "± 50,0",
            "± 70,0",
            "± 100,0",
            "± 150,0",
            "± 250,0",
            "± 350,0",
            "± 500,0",
            "± 700,0",
            "± 1000"});
            this.cb_dev1_fps1.Location = new System.Drawing.Point(149, 113);
            this.cb_dev1_fps1.Name = "cb_dev1_fps1";
            this.cb_dev1_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_dev1_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_dev1_fps1.TabIndex = 2;
            this.cb_dev1_fps1.Text = "±1,75";
            // 
            // cb_mod1_fps1
            // 
            this.cb_mod1_fps1.FormattingEnabled = true;
            this.cb_mod1_fps1.Items.AddRange(new object[] {
            "---",
            "ЧМШ",
            "ЧМ-2",
            "ЧМ-4",
            "ЧМ-8",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "КФМ"});
            this.cb_mod1_fps1.Location = new System.Drawing.Point(149, 89);
            this.cb_mod1_fps1.Name = "cb_mod1_fps1";
            this.cb_mod1_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod1_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_mod1_fps1.TabIndex = 2;
            this.cb_mod1_fps1.Text = "ЧМШ";
            this.cb_mod1_fps1.SelectedIndexChanged += new System.EventHandler(this.cb_mod1_fps1_SelectedIndexChanged);
            // 
            // cb_tim1_fps1
            // 
            this.cb_tim1_fps1.FormattingEnabled = true;
            this.cb_tim1_fps1.Items.AddRange(new object[] {
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim1_fps1.Location = new System.Drawing.Point(149, 162);
            this.cb_tim1_fps1.Name = "cb_tim1_fps1";
            this.cb_tim1_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim1_fps1.Size = new System.Drawing.Size(65, 21);
            this.cb_tim1_fps1.TabIndex = 2;
            this.cb_tim1_fps1.Text = "0,5 мс";
            // 
            // comboBox_freq_num_fps1
            // 
            this.comboBox_freq_num_fps1.FormattingEnabled = true;
            this.comboBox_freq_num_fps1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBox_freq_num_fps1.Location = new System.Drawing.Point(151, 40);
            this.comboBox_freq_num_fps1.Name = "comboBox_freq_num_fps1";
            this.comboBox_freq_num_fps1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox_freq_num_fps1.Size = new System.Drawing.Size(55, 21);
            this.comboBox_freq_num_fps1.TabIndex = 2;
            this.comboBox_freq_num_fps1.Text = "1";
            this.comboBox_freq_num_fps1.SelectedIndexChanged += new System.EventHandler(this.comboBox_freq_num_fps1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(10, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Код длительности [мс]:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Код манипуляции [мкс]:";
            // 
            // label_duration_code
            // 
            this.label_duration_code.AutoSize = true;
            this.label_duration_code.ForeColor = System.Drawing.Color.Black;
            this.label_duration_code.Location = new System.Drawing.Point(28, 117);
            this.label_duration_code.Name = "label_duration_code";
            this.label_duration_code.Size = new System.Drawing.Size(107, 13);
            this.label_duration_code.TabIndex = 1;
            this.label_duration_code.Text = "Код девиации [кГц]:";
            // 
            // tb_freq4_fps1
            // 
            this.tb_freq4_fps1.Enabled = false;
            this.tb_freq4_fps1.Location = new System.Drawing.Point(370, 65);
            this.tb_freq4_fps1.Name = "tb_freq4_fps1";
            this.tb_freq4_fps1.Size = new System.Drawing.Size(65, 20);
            this.tb_freq4_fps1.TabIndex = 0;
            this.tb_freq4_fps1.Text = "47000";
            // 
            // label_code_freq
            // 
            this.label_code_freq.AutoSize = true;
            this.label_code_freq.ForeColor = System.Drawing.Color.Black;
            this.label_code_freq.Location = new System.Drawing.Point(35, 68);
            this.label_code_freq.Name = "label_code_freq";
            this.label_code_freq.Size = new System.Drawing.Size(100, 13);
            this.label_code_freq.TabIndex = 1;
            this.label_code_freq.Text = "Код частоты [кГц]:";
            // 
            // tb_freq3_fps1
            // 
            this.tb_freq3_fps1.Enabled = false;
            this.tb_freq3_fps1.Location = new System.Drawing.Point(297, 65);
            this.tb_freq3_fps1.Name = "tb_freq3_fps1";
            this.tb_freq3_fps1.Size = new System.Drawing.Size(65, 20);
            this.tb_freq3_fps1.TabIndex = 0;
            this.tb_freq3_fps1.Text = "44000";
            // 
            // tb_freq2_fps1
            // 
            this.tb_freq2_fps1.Enabled = false;
            this.tb_freq2_fps1.Location = new System.Drawing.Point(222, 65);
            this.tb_freq2_fps1.Name = "tb_freq2_fps1";
            this.tb_freq2_fps1.Size = new System.Drawing.Size(65, 20);
            this.tb_freq2_fps1.TabIndex = 0;
            this.tb_freq2_fps1.Text = "32000";
            // 
            // label_freq_num
            // 
            this.label_freq_num.AutoSize = true;
            this.label_freq_num.ForeColor = System.Drawing.Color.Black;
            this.label_freq_num.Location = new System.Drawing.Point(40, 43);
            this.label_freq_num.Name = "label_freq_num";
            this.label_freq_num.Size = new System.Drawing.Size(95, 13);
            this.label_freq_num.TabIndex = 1;
            this.label_freq_num.Text = "Количество ИРИ:";
            // 
            // tb_freq1_fps1
            // 
            this.tb_freq1_fps1.Location = new System.Drawing.Point(149, 65);
            this.tb_freq1_fps1.Name = "tb_freq1_fps1";
            this.tb_freq1_fps1.Size = new System.Drawing.Size(65, 20);
            this.tb_freq1_fps1.TabIndex = 0;
            this.tb_freq1_fps1.Text = "31000";
            // 
            // label_nomer_pod
            // 
            this.label_nomer_pod.AutoSize = true;
            this.label_nomer_pod.ForeColor = System.Drawing.Color.Black;
            this.label_nomer_pod.Location = new System.Drawing.Point(88, 19);
            this.label_nomer_pod.Name = "label_nomer_pod";
            this.label_nomer_pod.Size = new System.Drawing.Size(47, 13);
            this.label_nomer_pod.TabIndex = 1;
            this.label_nomer_pod.Text = "Литера:";
            // 
            // label_band_code
            // 
            this.label_band_code.AutoSize = true;
            this.label_band_code.ForeColor = System.Drawing.Color.Black;
            this.label_band_code.Location = new System.Drawing.Point(48, 92);
            this.label_band_code.Name = "label_band_code";
            this.label_band_code.Size = new System.Drawing.Size(87, 13);
            this.label_band_code.TabIndex = 1;
            this.label_band_code.Text = "Код модуляции:";
            // 
            // button_power_off_FPS1
            // 
            this.button_power_off_FPS1.Enabled = false;
            this.button_power_off_FPS1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button_power_off_FPS1.Location = new System.Drawing.Point(507, 52);
            this.button_power_off_FPS1.Name = "button_power_off_FPS1";
            this.button_power_off_FPS1.Size = new System.Drawing.Size(78, 23);
            this.button_power_off_FPS1.TabIndex = 5;
            this.button_power_off_FPS1.Text = "Signal OFF";
            this.button_power_off_FPS1.UseVisualStyleBackColor = true;
            this.button_power_off_FPS1.Click += new System.EventHandler(this.button_power_off_FPS1_Click);
            // 
            // button_param_FPS1
            // 
            this.button_param_FPS1.Enabled = false;
            this.button_param_FPS1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_param_FPS1.Location = new System.Drawing.Point(507, 83);
            this.button_param_FPS1.Name = "button_param_FPS1";
            this.button_param_FPS1.Size = new System.Drawing.Size(78, 23);
            this.button_param_FPS1.TabIndex = 5;
            this.button_param_FPS1.Text = "Set Param";
            this.button_param_FPS1.UseVisualStyleBackColor = true;
            this.button_param_FPS1.Click += new System.EventHandler(this.button_param_FPS1_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.listBoxErr);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.labelGetNv);
            this.tabPage3.Controls.Add(this.labelGetNn);
            this.tabPage3.Controls.Add(this.labeVer);
            this.tabPage3.Controls.Add(this.button_Get_Ver_FPS2);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.labelParamK);
            this.tabPage3.Controls.Add(this.button_GetAmp_FPS2);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.tb_ampN_fps2);
            this.tabPage3.Controls.Add(this.button_power2_off_FPS2);
            this.tabPage3.Controls.Add(this.button_Test_FPS2);
            this.tabPage3.Controls.Add(this.tb_ampV_fps2);
            this.tabPage3.Controls.Add(this.button_Amp_FPS2);
            this.tabPage3.Controls.Add(this.button_power_off_all_FPS2);
            this.tabPage3.Controls.Add(this.button_status_on_FPS2);
            this.tabPage3.Controls.Add(this.button_power_on_FPS2);
            this.tabPage3.Controls.Add(this.tb_dev2_fps2);
            this.tabPage3.Controls.Add(this.checkBox_GPS_L2);
            this.tabPage3.Controls.Add(this.checkBox_GPS_L1);
            this.tabPage3.Controls.Add(this.checkBox_GLONASS_L2);
            this.tabPage3.Controls.Add(this.checkBox_GLONASS_L1);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.tb_dev1_fps2);
            this.tabPage3.Controls.Add(this.tb_man2_fps2);
            this.tabPage3.Controls.Add(this.tb_man1_fps2);
            this.tabPage3.Controls.Add(this.cb_tim2_fps2);
            this.tabPage3.Controls.Add(this.cb_man2_fps2);
            this.tabPage3.Controls.Add(this.cb_mod2_fps2);
            this.tabPage3.Controls.Add(this.cb_man1_fps2);
            this.tabPage3.Controls.Add(this.cb_mod1_fps2);
            this.tabPage3.Controls.Add(this.cb_tim1_fps2);
            this.tabPage3.Controls.Add(this.comboBox_freq_num_fps2);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.tb_freq2_fps2);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.tb_freq1_fps2);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.button_param_FPS2);
            this.tabPage3.Controls.Add(this.button_power_off_FPS2);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.comboBox_litera_FPS2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1061, 387);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Управление ФПС2";
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // listBoxErr
            // 
            this.listBoxErr.FormattingEnabled = true;
            this.listBoxErr.Location = new System.Drawing.Point(772, 23);
            this.listBoxErr.Name = "listBoxErr";
            this.listBoxErr.ScrollAlwaysVisible = true;
            this.listBoxErr.Size = new System.Drawing.Size(179, 134);
            this.listBoxErr.TabIndex = 79;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(769, 3);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 13);
            this.label24.TabIndex = 78;
            this.label24.Text = "Статус:";
            // 
            // labelGetNv
            // 
            this.labelGetNv.AutoSize = true;
            this.labelGetNv.ForeColor = System.Drawing.Color.Black;
            this.labelGetNv.Location = new System.Drawing.Point(717, 145);
            this.labelGetNv.Name = "labelGetNv";
            this.labelGetNv.Size = new System.Drawing.Size(10, 13);
            this.labelGetNv.TabIndex = 76;
            this.labelGetNv.Text = "-";
            // 
            // labelGetNn
            // 
            this.labelGetNn.AutoSize = true;
            this.labelGetNn.ForeColor = System.Drawing.Color.Black;
            this.labelGetNn.Location = new System.Drawing.Point(717, 111);
            this.labelGetNn.Name = "labelGetNn";
            this.labelGetNn.Size = new System.Drawing.Size(10, 13);
            this.labelGetNn.TabIndex = 75;
            this.labelGetNn.Text = "-";
            // 
            // labeVer
            // 
            this.labeVer.AutoSize = true;
            this.labeVer.ForeColor = System.Drawing.Color.Black;
            this.labeVer.Location = new System.Drawing.Point(769, 207);
            this.labeVer.Name = "labeVer";
            this.labeVer.Size = new System.Drawing.Size(28, 13);
            this.labeVer.TabIndex = 74;
            this.labeVer.Text = "ver=";
            // 
            // button_Get_Ver_FPS2
            // 
            this.button_Get_Ver_FPS2.Enabled = false;
            this.button_Get_Ver_FPS2.ForeColor = System.Drawing.Color.Fuchsia;
            this.button_Get_Ver_FPS2.Location = new System.Drawing.Point(772, 173);
            this.button_Get_Ver_FPS2.Name = "button_Get_Ver_FPS2";
            this.button_Get_Ver_FPS2.Size = new System.Drawing.Size(78, 23);
            this.button_Get_Ver_FPS2.TabIndex = 73;
            this.button_Get_Ver_FPS2.Text = "Get Ver";
            this.button_Get_Ver_FPS2.UseVisualStyleBackColor = true;
            this.button_Get_Ver_FPS2.Click += new System.EventHandler(this.button_Get_Ver_FPS2_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(508, 178);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 13);
            this.label22.TabIndex = 72;
            this.label22.Text = "Параметр К =";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(628, 256);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 71;
            // 
            // labelParamK
            // 
            this.labelParamK.AutoSize = true;
            this.labelParamK.ForeColor = System.Drawing.Color.Black;
            this.labelParamK.Location = new System.Drawing.Point(602, 178);
            this.labelParamK.Name = "labelParamK";
            this.labelParamK.Size = new System.Drawing.Size(10, 13);
            this.labelParamK.TabIndex = 70;
            this.labelParamK.Text = "-";
            this.labelParamK.Click += new System.EventHandler(this.labelParamK_Click);
            // 
            // button_GetAmp_FPS2
            // 
            this.button_GetAmp_FPS2.Enabled = false;
            this.button_GetAmp_FPS2.ForeColor = System.Drawing.Color.Fuchsia;
            this.button_GetAmp_FPS2.Location = new System.Drawing.Point(689, 74);
            this.button_GetAmp_FPS2.Name = "button_GetAmp_FPS2";
            this.button_GetAmp_FPS2.Size = new System.Drawing.Size(59, 23);
            this.button_GetAmp_FPS2.TabIndex = 69;
            this.button_GetAmp_FPS2.Text = "Get Amp";
            this.button_GetAmp_FPS2.UseVisualStyleBackColor = true;
            this.button_GetAmp_FPS2.Click += new System.EventHandler(this.button_GetAmp_FPS2_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(676, 144);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 13);
            this.label21.TabIndex = 68;
            this.label21.Text = "L2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(676, 111);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 67;
            this.label20.Text = "L1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(511, 144);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 66;
            this.label19.Text = "Усиление Nv";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(514, 111);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 65;
            this.label18.Text = "Усиление Nn";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // tb_ampN_fps2
            // 
            this.tb_ampN_fps2.Location = new System.Drawing.Point(605, 108);
            this.tb_ampN_fps2.Name = "tb_ampN_fps2";
            this.tb_ampN_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_ampN_fps2.TabIndex = 64;
            this.tb_ampN_fps2.Text = "30";
            // 
            // button_power2_off_FPS2
            // 
            this.button_power2_off_FPS2.Enabled = false;
            this.button_power2_off_FPS2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button_power2_off_FPS2.Location = new System.Drawing.Point(426, 74);
            this.button_power2_off_FPS2.Name = "button_power2_off_FPS2";
            this.button_power2_off_FPS2.Size = new System.Drawing.Size(67, 23);
            this.button_power2_off_FPS2.TabIndex = 63;
            this.button_power2_off_FPS2.Text = "Sig 2 OFF";
            this.button_power2_off_FPS2.UseVisualStyleBackColor = true;
            this.button_power2_off_FPS2.Click += new System.EventHandler(this.button_power2_off_FPS2_Click);
            // 
            // button_Test_FPS2
            // 
            this.button_Test_FPS2.Enabled = false;
            this.button_Test_FPS2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_Test_FPS2.Location = new System.Drawing.Point(605, 23);
            this.button_Test_FPS2.Name = "button_Test_FPS2";
            this.button_Test_FPS2.Size = new System.Drawing.Size(58, 23);
            this.button_Test_FPS2.TabIndex = 60;
            this.button_Test_FPS2.Text = "Set Test";
            this.button_Test_FPS2.UseVisualStyleBackColor = true;
            this.button_Test_FPS2.Click += new System.EventHandler(this.button_Test_FPS2_Click);
            // 
            // tb_ampV_fps2
            // 
            this.tb_ampV_fps2.Location = new System.Drawing.Point(605, 141);
            this.tb_ampV_fps2.Name = "tb_ampV_fps2";
            this.tb_ampV_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_ampV_fps2.TabIndex = 59;
            this.tb_ampV_fps2.Text = "30";
            // 
            // button_Amp_FPS2
            // 
            this.button_Amp_FPS2.Enabled = false;
            this.button_Amp_FPS2.ForeColor = System.Drawing.Color.Fuchsia;
            this.button_Amp_FPS2.Location = new System.Drawing.Point(605, 74);
            this.button_Amp_FPS2.Name = "button_Amp_FPS2";
            this.button_Amp_FPS2.Size = new System.Drawing.Size(58, 23);
            this.button_Amp_FPS2.TabIndex = 57;
            this.button_Amp_FPS2.Text = "Set Amp";
            this.button_Amp_FPS2.UseVisualStyleBackColor = true;
            this.button_Amp_FPS2.Click += new System.EventHandler(this.button_Amp_FPS2_Click);
            // 
            // button_power_off_all_FPS2
            // 
            this.button_power_off_all_FPS2.Enabled = false;
            this.button_power_off_all_FPS2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button_power_off_all_FPS2.Location = new System.Drawing.Point(510, 74);
            this.button_power_off_all_FPS2.Name = "button_power_off_all_FPS2";
            this.button_power_off_all_FPS2.Size = new System.Drawing.Size(75, 23);
            this.button_power_off_all_FPS2.TabIndex = 56;
            this.button_power_off_all_FPS2.Text = "Sig all OFF";
            this.button_power_off_all_FPS2.UseVisualStyleBackColor = true;
            this.button_power_off_all_FPS2.Click += new System.EventHandler(this.button_power_off_all_FPS2_Click);
            // 
            // button_status_on_FPS2
            // 
            this.button_status_on_FPS2.Enabled = false;
            this.button_status_on_FPS2.ForeColor = System.Drawing.Color.Brown;
            this.button_status_on_FPS2.Location = new System.Drawing.Point(426, 23);
            this.button_status_on_FPS2.Name = "button_status_on_FPS2";
            this.button_status_on_FPS2.Size = new System.Drawing.Size(67, 23);
            this.button_status_on_FPS2.TabIndex = 53;
            this.button_status_on_FPS2.Text = "Status";
            this.button_status_on_FPS2.UseVisualStyleBackColor = true;
            this.button_status_on_FPS2.Click += new System.EventHandler(this.button_status_on_FPS2_Click);
            // 
            // button_power_on_FPS2
            // 
            this.button_power_on_FPS2.Enabled = false;
            this.button_power_on_FPS2.ForeColor = System.Drawing.Color.Green;
            this.button_power_on_FPS2.Location = new System.Drawing.Point(510, 23);
            this.button_power_on_FPS2.Name = "button_power_on_FPS2";
            this.button_power_on_FPS2.Size = new System.Drawing.Size(75, 23);
            this.button_power_on_FPS2.TabIndex = 51;
            this.button_power_on_FPS2.Text = "Signal ON";
            this.button_power_on_FPS2.UseVisualStyleBackColor = true;
            this.button_power_on_FPS2.Click += new System.EventHandler(this.button_power_on_FPS2_Click);
            // 
            // tb_dev2_fps2
            // 
            this.tb_dev2_fps2.Location = new System.Drawing.Point(235, 123);
            this.tb_dev2_fps2.Name = "tb_dev2_fps2";
            this.tb_dev2_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_dev2_fps2.TabIndex = 52;
            this.tb_dev2_fps2.Text = "----";
            // 
            // checkBox_GPS_L2
            // 
            this.checkBox_GPS_L2.AutoSize = true;
            this.checkBox_GPS_L2.Enabled = false;
            this.checkBox_GPS_L2.Location = new System.Drawing.Point(426, 143);
            this.checkBox_GPS_L2.Name = "checkBox_GPS_L2";
            this.checkBox_GPS_L2.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GPS_L2.TabIndex = 51;
            this.checkBox_GPS_L2.Text = "L2";
            this.checkBox_GPS_L2.UseVisualStyleBackColor = true;
            // 
            // checkBox_GPS_L1
            // 
            this.checkBox_GPS_L1.AutoSize = true;
            this.checkBox_GPS_L1.Enabled = false;
            this.checkBox_GPS_L1.Location = new System.Drawing.Point(351, 144);
            this.checkBox_GPS_L1.Name = "checkBox_GPS_L1";
            this.checkBox_GPS_L1.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GPS_L1.TabIndex = 50;
            this.checkBox_GPS_L1.Text = "L1";
            this.checkBox_GPS_L1.UseVisualStyleBackColor = true;
            // 
            // checkBox_GLONASS_L2
            // 
            this.checkBox_GLONASS_L2.AutoSize = true;
            this.checkBox_GLONASS_L2.Enabled = false;
            this.checkBox_GLONASS_L2.Location = new System.Drawing.Point(426, 206);
            this.checkBox_GLONASS_L2.Name = "checkBox_GLONASS_L2";
            this.checkBox_GLONASS_L2.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GLONASS_L2.TabIndex = 49;
            this.checkBox_GLONASS_L2.Text = "L2";
            this.checkBox_GLONASS_L2.UseVisualStyleBackColor = true;
            // 
            // checkBox_GLONASS_L1
            // 
            this.checkBox_GLONASS_L1.AutoSize = true;
            this.checkBox_GLONASS_L1.Enabled = false;
            this.checkBox_GLONASS_L1.Location = new System.Drawing.Point(351, 207);
            this.checkBox_GLONASS_L1.Name = "checkBox_GLONASS_L1";
            this.checkBox_GLONASS_L1.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GLONASS_L1.TabIndex = 48;
            this.checkBox_GLONASS_L1.Text = "L1";
            this.checkBox_GLONASS_L1.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(6, 174);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "Время сканирования [кГц]:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(329, 115);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(135, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Система навигации GPS:";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(329, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Система навигации ГЛОНАСС:";
            // 
            // tb_dev1_fps2
            // 
            this.tb_dev1_fps2.Location = new System.Drawing.Point(162, 123);
            this.tb_dev1_fps2.Name = "tb_dev1_fps2";
            this.tb_dev1_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_dev1_fps2.TabIndex = 37;
            this.tb_dev1_fps2.Text = "----";
            // 
            // tb_man2_fps2
            // 
            this.tb_man2_fps2.Enabled = false;
            this.tb_man2_fps2.Location = new System.Drawing.Point(235, 171);
            this.tb_man2_fps2.Name = "tb_man2_fps2";
            this.tb_man2_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_man2_fps2.TabIndex = 34;
            this.tb_man2_fps2.Text = "----";
            // 
            // tb_man1_fps2
            // 
            this.tb_man1_fps2.Location = new System.Drawing.Point(162, 171);
            this.tb_man1_fps2.Name = "tb_man1_fps2";
            this.tb_man1_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_man1_fps2.TabIndex = 35;
            this.tb_man1_fps2.Text = "----";
            // 
            // cb_tim2_fps2
            // 
            this.cb_tim2_fps2.Enabled = false;
            this.cb_tim2_fps2.FormattingEnabled = true;
            this.cb_tim2_fps2.Items.AddRange(new object[] {
            "Непр.",
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim2_fps2.Location = new System.Drawing.Point(235, 195);
            this.cb_tim2_fps2.Name = "cb_tim2_fps2";
            this.cb_tim2_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim2_fps2.Size = new System.Drawing.Size(65, 21);
            this.cb_tim2_fps2.TabIndex = 31;
            this.cb_tim2_fps2.Text = "Непр.";
            // 
            // cb_man2_fps2
            // 
            this.cb_man2_fps2.Enabled = false;
            this.cb_man2_fps2.FormattingEnabled = true;
            this.cb_man2_fps2.Items.AddRange(new object[] {
            "----"});
            this.cb_man2_fps2.Location = new System.Drawing.Point(235, 146);
            this.cb_man2_fps2.Name = "cb_man2_fps2";
            this.cb_man2_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man2_fps2.Size = new System.Drawing.Size(65, 21);
            this.cb_man2_fps2.TabIndex = 24;
            this.cb_man2_fps2.Text = "----";
            // 
            // cb_mod2_fps2
            // 
            this.cb_mod2_fps2.Enabled = false;
            this.cb_mod2_fps2.FormattingEnabled = true;
            this.cb_mod2_fps2.Items.AddRange(new object[] {
            "----",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "ЛЧМ"});
            this.cb_mod2_fps2.Location = new System.Drawing.Point(235, 98);
            this.cb_mod2_fps2.Name = "cb_mod2_fps2";
            this.cb_mod2_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod2_fps2.Size = new System.Drawing.Size(65, 21);
            this.cb_mod2_fps2.TabIndex = 22;
            this.cb_mod2_fps2.Text = "----";
            this.cb_mod2_fps2.SelectedIndexChanged += new System.EventHandler(this.cb_mod2_fps2_SelectedIndexChanged);
            // 
            // cb_man1_fps2
            // 
            this.cb_man1_fps2.FormattingEnabled = true;
            this.cb_man1_fps2.Items.AddRange(new object[] {
            "----"});
            this.cb_man1_fps2.Location = new System.Drawing.Point(162, 146);
            this.cb_man1_fps2.Name = "cb_man1_fps2";
            this.cb_man1_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man1_fps2.Size = new System.Drawing.Size(65, 21);
            this.cb_man1_fps2.TabIndex = 33;
            this.cb_man1_fps2.Text = "----";
            // 
            // cb_mod1_fps2
            // 
            this.cb_mod1_fps2.FormattingEnabled = true;
            this.cb_mod1_fps2.Items.AddRange(new object[] {
            "----",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "ЛЧМ"});
            this.cb_mod1_fps2.Location = new System.Drawing.Point(162, 98);
            this.cb_mod1_fps2.Name = "cb_mod1_fps2";
            this.cb_mod1_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod1_fps2.Size = new System.Drawing.Size(65, 21);
            this.cb_mod1_fps2.TabIndex = 20;
            this.cb_mod1_fps2.Text = "----";
            this.cb_mod1_fps2.SelectedIndexChanged += new System.EventHandler(this.cb_mod1_fps2_SelectedIndexChanged);
            // 
            // cb_tim1_fps2
            // 
            this.cb_tim1_fps2.FormattingEnabled = true;
            this.cb_tim1_fps2.Items.AddRange(new object[] {
            "Непр.",
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim1_fps2.Location = new System.Drawing.Point(162, 195);
            this.cb_tim1_fps2.Name = "cb_tim1_fps2";
            this.cb_tim1_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim1_fps2.Size = new System.Drawing.Size(65, 21);
            this.cb_tim1_fps2.TabIndex = 19;
            this.cb_tim1_fps2.Text = "Непр.";
            // 
            // comboBox_freq_num_fps2
            // 
            this.comboBox_freq_num_fps2.FormattingEnabled = true;
            this.comboBox_freq_num_fps2.Items.AddRange(new object[] {
            "1",
            "2"});
            this.comboBox_freq_num_fps2.Location = new System.Drawing.Point(164, 49);
            this.comboBox_freq_num_fps2.Name = "comboBox_freq_num_fps2";
            this.comboBox_freq_num_fps2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox_freq_num_fps2.Size = new System.Drawing.Size(55, 21);
            this.comboBox_freq_num_fps2.TabIndex = 18;
            this.comboBox_freq_num_fps2.Text = "1";
            this.comboBox_freq_num_fps2.SelectedIndexChanged += new System.EventHandler(this.comboBox_freq_num_fps2_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(26, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Код длительности [мс]:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(22, 149);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Код манипуляции [мкс]:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(44, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Код девиации [кГц]:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(51, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Код частоты [кГц]:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // tb_freq2_fps2
            // 
            this.tb_freq2_fps2.Enabled = false;
            this.tb_freq2_fps2.Location = new System.Drawing.Point(235, 74);
            this.tb_freq2_fps2.Name = "tb_freq2_fps2";
            this.tb_freq2_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_freq2_fps2.TabIndex = 8;
            this.tb_freq2_fps2.Text = "150000";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(54, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Количество ИРИ:";
            // 
            // tb_freq1_fps2
            // 
            this.tb_freq1_fps2.Location = new System.Drawing.Point(162, 74);
            this.tb_freq1_fps2.Name = "tb_freq1_fps2";
            this.tb_freq1_fps2.Size = new System.Drawing.Size(65, 20);
            this.tb_freq1_fps2.TabIndex = 11;
            this.tb_freq1_fps2.Text = "150000";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(64, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Код модуляции:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // button_param_FPS2
            // 
            this.button_param_FPS2.Enabled = false;
            this.button_param_FPS2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button_param_FPS2.Location = new System.Drawing.Point(332, 23);
            this.button_param_FPS2.Name = "button_param_FPS2";
            this.button_param_FPS2.Size = new System.Drawing.Size(78, 23);
            this.button_param_FPS2.TabIndex = 7;
            this.button_param_FPS2.Text = "Set Param";
            this.button_param_FPS2.UseVisualStyleBackColor = true;
            this.button_param_FPS2.Click += new System.EventHandler(this.button_param_FPS2_Click);
            // 
            // button_power_off_FPS2
            // 
            this.button_power_off_FPS2.Enabled = false;
            this.button_power_off_FPS2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button_power_off_FPS2.Location = new System.Drawing.Point(332, 74);
            this.button_power_off_FPS2.Name = "button_power_off_FPS2";
            this.button_power_off_FPS2.Size = new System.Drawing.Size(69, 23);
            this.button_power_off_FPS2.TabIndex = 6;
            this.button_power_off_FPS2.Text = "Sig1 OFF";
            this.button_power_off_FPS2.UseVisualStyleBackColor = true;
            this.button_power_off_FPS2.Click += new System.EventHandler(this.button_power_off_FPS2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(101, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Литера:";
            // 
            // comboBox_litera_FPS2
            // 
            this.comboBox_litera_FPS2.FormattingEnabled = true;
            this.comboBox_litera_FPS2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBox_litera_FPS2.Location = new System.Drawing.Point(164, 22);
            this.comboBox_litera_FPS2.Name = "comboBox_litera_FPS2";
            this.comboBox_litera_FPS2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox_litera_FPS2.Size = new System.Drawing.Size(136, 21);
            this.comboBox_litera_FPS2.TabIndex = 3;
            this.comboBox_litera_FPS2.Text = "1";
            this.comboBox_litera_FPS2.SelectedIndexChanged += new System.EventHandler(this.comboBox_litera_FPS2_SelectedIndexChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.saveParamAtBooster);
            this.tabPage5.Controls.Add(this.getParams);
            this.tabPage5.Controls.Add(this.button7);
            this.tabPage5.Controls.Add(this.button6);
            this.tabPage5.Controls.Add(this.button5);
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.dataGridView1);
            this.tabPage5.Controls.Add(this.label47);
            this.tabPage5.Controls.Add(this.cb_literaCH_fps3);
            this.tabPage5.Controls.Add(this.label44);
            this.tabPage5.Controls.Add(this.listBoxErr_fps3);
            this.tabPage5.Controls.Add(this.label26);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.label29);
            this.tabPage5.Controls.Add(this.bt_Get_Ver_FPS2);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.label32);
            this.tabPage5.Controls.Add(this.bt_GetAmp_fps3);
            this.tabPage5.Controls.Add(this.label33);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.label35);
            this.tabPage5.Controls.Add(this.label25);
            this.tabPage5.Controls.Add(this.label36);
            this.tabPage5.Controls.Add(this.tb_ampNn_fps3);
            this.tabPage5.Controls.Add(this.tb_ampNv_fps3);
            this.tabPage5.Controls.Add(this.bt_Amp_fps3);
            this.tabPage5.Controls.Add(this.bt_get_status_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GPS_L2_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GPS_L1_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GL_L2_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GL_L1_fps3);
            this.tabPage5.Controls.Add(this.label37);
            this.tabPage5.Controls.Add(this.label38);
            this.tabPage5.Controls.Add(this.label39);
            this.tabPage5.Controls.Add(this.tb_dev1_fps3);
            this.tabPage5.Controls.Add(this.tb_man1_fps3);
            this.tabPage5.Controls.Add(this.cb_man1_fps3);
            this.tabPage5.Controls.Add(this.cb_mod1_fps3);
            this.tabPage5.Controls.Add(this.cb_tim1_fps3);
            this.tabPage5.Controls.Add(this.label40);
            this.tabPage5.Controls.Add(this.label41);
            this.tabPage5.Controls.Add(this.label42);
            this.tabPage5.Controls.Add(this.label43);
            this.tabPage5.Controls.Add(this.tb_freq2_fps3);
            this.tabPage5.Controls.Add(this.tb_freq1_fps3);
            this.tabPage5.Controls.Add(this.label45);
            this.tabPage5.Controls.Add(this.bt_set_param_FPS3);
            this.tabPage5.Controls.Add(this.bt_power_off_All_FPS3);
            this.tabPage5.Controls.Add(this.bt_power_off_FPS3);
            this.tabPage5.Controls.Add(this.label46);
            this.tabPage5.Controls.Add(this.cb_literaK_fps3);
            this.tabPage5.Controls.Add(this.cb_litera_fps3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1061, 387);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Управление ФПС3";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // saveParamAtBooster
            // 
            this.saveParamAtBooster.Enabled = false;
            this.saveParamAtBooster.ForeColor = System.Drawing.Color.Fuchsia;
            this.saveParamAtBooster.Location = new System.Drawing.Point(434, 298);
            this.saveParamAtBooster.Name = "saveParamAtBooster";
            this.saveParamAtBooster.Size = new System.Drawing.Size(83, 35);
            this.saveParamAtBooster.TabIndex = 142;
            this.saveParamAtBooster.Text = "Save param at booster";
            this.saveParamAtBooster.UseVisualStyleBackColor = true;
            this.saveParamAtBooster.Click += new System.EventHandler(this.saveParamAtBooster_Click);
            // 
            // getParams
            // 
            this.getParams.Enabled = false;
            this.getParams.ForeColor = System.Drawing.Color.Fuchsia;
            this.getParams.Location = new System.Drawing.Point(434, 339);
            this.getParams.Name = "getParams";
            this.getParams.Size = new System.Drawing.Size(83, 35);
            this.getParams.TabIndex = 141;
            this.getParams.Text = "Get Params";
            this.getParams.UseVisualStyleBackColor = true;
            this.getParams.Click += new System.EventHandler(this.getParams_Click);
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.ForeColor = System.Drawing.Color.Fuchsia;
            this.button7.Location = new System.Drawing.Point(434, 255);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 35);
            this.button7.TabIndex = 140;
            this.button7.Text = "Clean up table";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.CleanUpButton_Click);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.ForeColor = System.Drawing.Color.Fuchsia;
            this.button6.Location = new System.Drawing.Point(535, 257);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(83, 35);
            this.button6.TabIndex = 139;
            this.button6.Text = "Load table from";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.ForeColor = System.Drawing.Color.Fuchsia;
            this.button5.Location = new System.Drawing.Point(535, 220);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 35);
            this.button5.TabIndex = 138;
            this.button5.Text = "Save table to";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.ForeColor = System.Drawing.Color.Fuchsia;
            this.button4.Location = new System.Drawing.Point(535, 298);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 35);
            this.button4.TabIndex = 137;
            this.button4.Text = "Remove Params";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_Remove_Params);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.ForeColor = System.Drawing.Color.Fuchsia;
            this.button3.Location = new System.Drawing.Point(535, 339);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 35);
            this.button3.TabIndex = 136;
            this.button3.Text = "Write Params";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_Write_Params);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(627, 233);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(426, 152);
            this.dataGridView1.TabIndex = 135;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(382, 57);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(59, 13);
            this.label47.TabIndex = 134;
            this.label47.Text = "Номер CH";
            // 
            // cb_literaCH_fps3
            // 
            this.cb_literaCH_fps3.FormattingEnabled = true;
            this.cb_literaCH_fps3.Items.AddRange(new object[] {
            "1 | 100-500",
            "2 | 500-2500",
            "3 | 2500-6000",
            "4 | GNSS",
            "5 | All"});
            this.cb_literaCH_fps3.Location = new System.Drawing.Point(385, 73);
            this.cb_literaCH_fps3.Name = "cb_literaCH_fps3";
            this.cb_literaCH_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_literaCH_fps3.Size = new System.Drawing.Size(160, 21);
            this.cb_literaCH_fps3.TabIndex = 133;
            this.cb_literaCH_fps3.Text = "1 | 100-500";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(214, 86);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(130, 13);
            this.label44.TabIndex = 132;
            this.label44.Text = "Частота Nn/Частота Nv";
            // 
            // listBoxErr_fps3
            // 
            this.listBoxErr_fps3.FormattingEnabled = true;
            this.listBoxErr_fps3.Location = new System.Drawing.Point(824, 21);
            this.listBoxErr_fps3.Name = "listBoxErr_fps3";
            this.listBoxErr_fps3.ScrollAlwaysVisible = true;
            this.listBoxErr_fps3.Size = new System.Drawing.Size(179, 134);
            this.listBoxErr_fps3.TabIndex = 131;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(821, 1);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 13);
            this.label26.TabIndex = 130;
            this.label26.Text = "Статус:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(770, 171);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(10, 13);
            this.label27.TabIndex = 129;
            this.label27.Text = "-";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(770, 137);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(10, 13);
            this.label28.TabIndex = 128;
            this.label28.Text = "-";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(821, 205);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(28, 13);
            this.label29.TabIndex = 127;
            this.label29.Text = "ver=";
            // 
            // bt_Get_Ver_FPS2
            // 
            this.bt_Get_Ver_FPS2.Enabled = false;
            this.bt_Get_Ver_FPS2.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_Get_Ver_FPS2.Location = new System.Drawing.Point(824, 171);
            this.bt_Get_Ver_FPS2.Name = "bt_Get_Ver_FPS2";
            this.bt_Get_Ver_FPS2.Size = new System.Drawing.Size(78, 23);
            this.bt_Get_Ver_FPS2.TabIndex = 126;
            this.bt_Get_Ver_FPS2.Text = "Get Ver";
            this.bt_Get_Ver_FPS2.UseVisualStyleBackColor = true;
            this.bt_Get_Ver_FPS2.Click += new System.EventHandler(this.bt_Get_Ver_FPS2_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(564, 199);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(77, 13);
            this.label30.TabIndex = 125;
            this.label30.Text = "Параметр К =";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(655, 199);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(10, 13);
            this.label32.TabIndex = 123;
            this.label32.Text = "-";
            // 
            // bt_GetAmp_fps3
            // 
            this.bt_GetAmp_fps3.Enabled = false;
            this.bt_GetAmp_fps3.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_GetAmp_fps3.Location = new System.Drawing.Point(744, 20);
            this.bt_GetAmp_fps3.Name = "bt_GetAmp_fps3";
            this.bt_GetAmp_fps3.Size = new System.Drawing.Size(59, 23);
            this.bt_GetAmp_fps3.TabIndex = 122;
            this.bt_GetAmp_fps3.Text = "Get Amp";
            this.bt_GetAmp_fps3.UseVisualStyleBackColor = true;
            this.bt_GetAmp_fps3.Click += new System.EventHandler(this.bt_GetAmp_fps3_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(729, 170);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(19, 13);
            this.label33.TabIndex = 121;
            this.label33.Text = "L2";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(729, 137);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 120;
            this.label34.Text = "L1";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(564, 170);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 13);
            this.label35.TabIndex = 119;
            this.label35.Text = "Усиление Nv";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(567, 89);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(51, 13);
            this.label25.TabIndex = 118;
            this.label25.Text = "Номер K";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(567, 137);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(74, 13);
            this.label36.TabIndex = 118;
            this.label36.Text = "Усиление Nn";
            // 
            // tb_ampNn_fps3
            // 
            this.tb_ampNn_fps3.Location = new System.Drawing.Point(658, 134);
            this.tb_ampNn_fps3.Name = "tb_ampNn_fps3";
            this.tb_ampNn_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_ampNn_fps3.TabIndex = 117;
            this.tb_ampNn_fps3.Text = "30";
            // 
            // tb_ampNv_fps3
            // 
            this.tb_ampNv_fps3.Location = new System.Drawing.Point(658, 167);
            this.tb_ampNv_fps3.Name = "tb_ampNv_fps3";
            this.tb_ampNv_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_ampNv_fps3.TabIndex = 114;
            this.tb_ampNv_fps3.Text = "30";
            // 
            // bt_Amp_fps3
            // 
            this.bt_Amp_fps3.Enabled = false;
            this.bt_Amp_fps3.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_Amp_fps3.Location = new System.Drawing.Point(657, 20);
            this.bt_Amp_fps3.Name = "bt_Amp_fps3";
            this.bt_Amp_fps3.Size = new System.Drawing.Size(58, 23);
            this.bt_Amp_fps3.TabIndex = 113;
            this.bt_Amp_fps3.Text = "Set Amp";
            this.bt_Amp_fps3.UseVisualStyleBackColor = true;
            this.bt_Amp_fps3.Click += new System.EventHandler(this.bt_Amp_fps3_Click);
            // 
            // bt_get_status_fps3
            // 
            this.bt_get_status_fps3.Enabled = false;
            this.bt_get_status_fps3.ForeColor = System.Drawing.Color.Brown;
            this.bt_get_status_fps3.Location = new System.Drawing.Point(478, 21);
            this.bt_get_status_fps3.Name = "bt_get_status_fps3";
            this.bt_get_status_fps3.Size = new System.Drawing.Size(67, 23);
            this.bt_get_status_fps3.TabIndex = 111;
            this.bt_get_status_fps3.Text = "Status";
            this.bt_get_status_fps3.UseVisualStyleBackColor = true;
            this.bt_get_status_fps3.Click += new System.EventHandler(this.bt_get_status_fps3_Click);
            // 
            // checkBox_GPS_L2_fps3
            // 
            this.checkBox_GPS_L2_fps3.AutoSize = true;
            this.checkBox_GPS_L2_fps3.Enabled = false;
            this.checkBox_GPS_L2_fps3.Location = new System.Drawing.Point(479, 169);
            this.checkBox_GPS_L2_fps3.Name = "checkBox_GPS_L2_fps3";
            this.checkBox_GPS_L2_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GPS_L2_fps3.TabIndex = 108;
            this.checkBox_GPS_L2_fps3.Text = "L2";
            this.checkBox_GPS_L2_fps3.UseVisualStyleBackColor = true;
            // 
            // checkBox_GPS_L1_fps3
            // 
            this.checkBox_GPS_L1_fps3.AutoSize = true;
            this.checkBox_GPS_L1_fps3.Enabled = false;
            this.checkBox_GPS_L1_fps3.Location = new System.Drawing.Point(404, 170);
            this.checkBox_GPS_L1_fps3.Name = "checkBox_GPS_L1_fps3";
            this.checkBox_GPS_L1_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GPS_L1_fps3.TabIndex = 107;
            this.checkBox_GPS_L1_fps3.Text = "L1";
            this.checkBox_GPS_L1_fps3.UseVisualStyleBackColor = true;
            // 
            // checkBox_GL_L2_fps3
            // 
            this.checkBox_GL_L2_fps3.AutoSize = true;
            this.checkBox_GL_L2_fps3.Enabled = false;
            this.checkBox_GL_L2_fps3.Location = new System.Drawing.Point(479, 232);
            this.checkBox_GL_L2_fps3.Name = "checkBox_GL_L2_fps3";
            this.checkBox_GL_L2_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GL_L2_fps3.TabIndex = 106;
            this.checkBox_GL_L2_fps3.Text = "L2";
            this.checkBox_GL_L2_fps3.UseVisualStyleBackColor = true;
            // 
            // checkBox_GL_L1_fps3
            // 
            this.checkBox_GL_L1_fps3.AutoSize = true;
            this.checkBox_GL_L1_fps3.Enabled = false;
            this.checkBox_GL_L1_fps3.Location = new System.Drawing.Point(404, 233);
            this.checkBox_GL_L1_fps3.Name = "checkBox_GL_L1_fps3";
            this.checkBox_GL_L1_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GL_L1_fps3.TabIndex = 105;
            this.checkBox_GL_L1_fps3.Text = "L1";
            this.checkBox_GL_L1_fps3.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(61, 202);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(145, 13);
            this.label37.TabIndex = 104;
            this.label37.Text = "Время сканирования [кГц]:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(382, 141);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(135, 13);
            this.label38.TabIndex = 103;
            this.label38.Text = "Система навигации GPS:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(382, 204);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(164, 13);
            this.label39.TabIndex = 102;
            this.label39.Text = "Система навигации ГЛОНАСС:";
            // 
            // tb_dev1_fps3
            // 
            this.tb_dev1_fps3.Location = new System.Drawing.Point(217, 151);
            this.tb_dev1_fps3.Name = "tb_dev1_fps3";
            this.tb_dev1_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_dev1_fps3.TabIndex = 101;
            this.tb_dev1_fps3.Text = "----";
            // 
            // tb_man1_fps3
            // 
            this.tb_man1_fps3.Location = new System.Drawing.Point(217, 199);
            this.tb_man1_fps3.Name = "tb_man1_fps3";
            this.tb_man1_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_man1_fps3.TabIndex = 100;
            this.tb_man1_fps3.Text = "----";
            // 
            // cb_man1_fps3
            // 
            this.cb_man1_fps3.FormattingEnabled = true;
            this.cb_man1_fps3.Items.AddRange(new object[] {
            "----"});
            this.cb_man1_fps3.Location = new System.Drawing.Point(217, 174);
            this.cb_man1_fps3.Name = "cb_man1_fps3";
            this.cb_man1_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man1_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_man1_fps3.TabIndex = 98;
            this.cb_man1_fps3.Text = "----";
            // 
            // cb_mod1_fps3
            // 
            this.cb_mod1_fps3.FormattingEnabled = true;
            this.cb_mod1_fps3.Items.AddRange(new object[] {
            "----",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "ЛЧМ"});
            this.cb_mod1_fps3.Location = new System.Drawing.Point(217, 126);
            this.cb_mod1_fps3.Name = "cb_mod1_fps3";
            this.cb_mod1_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod1_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_mod1_fps3.TabIndex = 94;
            this.cb_mod1_fps3.Text = "----";
            this.cb_mod1_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_mod1_fps3_SelectedIndexChanged);
            // 
            // cb_tim1_fps3
            // 
            this.cb_tim1_fps3.FormattingEnabled = true;
            this.cb_tim1_fps3.Items.AddRange(new object[] {
            "Непр.",
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim1_fps3.Location = new System.Drawing.Point(217, 223);
            this.cb_tim1_fps3.Name = "cb_tim1_fps3";
            this.cb_tim1_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim1_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_tim1_fps3.TabIndex = 93;
            this.cb_tim1_fps3.Text = "Непр.";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(81, 226);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(125, 13);
            this.label40.TabIndex = 91;
            this.label40.Text = "Код длительности [мс]:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(77, 177);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(128, 13);
            this.label41.TabIndex = 90;
            this.label41.Text = "Код манипуляции [мкс]:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(99, 154);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(107, 13);
            this.label42.TabIndex = 89;
            this.label42.Text = "Код девиации [кГц]:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(106, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(100, 13);
            this.label43.TabIndex = 88;
            this.label43.Text = "Код частоты [кГц]:";
            // 
            // tb_freq2_fps3
            // 
            this.tb_freq2_fps3.Enabled = false;
            this.tb_freq2_fps3.Location = new System.Drawing.Point(290, 102);
            this.tb_freq2_fps3.Name = "tb_freq2_fps3";
            this.tb_freq2_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_freq2_fps3.TabIndex = 84;
            this.tb_freq2_fps3.Text = "150000";
            // 
            // tb_freq1_fps3
            // 
            this.tb_freq1_fps3.Location = new System.Drawing.Point(217, 102);
            this.tb_freq1_fps3.Name = "tb_freq1_fps3";
            this.tb_freq1_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_freq1_fps3.TabIndex = 85;
            this.tb_freq1_fps3.Text = "150000";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(119, 129);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(87, 13);
            this.label45.TabIndex = 86;
            this.label45.Text = "Код модуляции:";
            // 
            // bt_set_param_FPS3
            // 
            this.bt_set_param_FPS3.Enabled = false;
            this.bt_set_param_FPS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_set_param_FPS3.Location = new System.Drawing.Point(384, 21);
            this.bt_set_param_FPS3.Name = "bt_set_param_FPS3";
            this.bt_set_param_FPS3.Size = new System.Drawing.Size(78, 23);
            this.bt_set_param_FPS3.TabIndex = 83;
            this.bt_set_param_FPS3.Text = "Set Param";
            this.bt_set_param_FPS3.UseVisualStyleBackColor = true;
            this.bt_set_param_FPS3.Click += new System.EventHandler(this.bt_set_param_FPS3_Click);
            // 
            // bt_power_off_All_FPS3
            // 
            this.bt_power_off_All_FPS3.Enabled = false;
            this.bt_power_off_All_FPS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bt_power_off_All_FPS3.Location = new System.Drawing.Point(479, 100);
            this.bt_power_off_All_FPS3.Name = "bt_power_off_All_FPS3";
            this.bt_power_off_All_FPS3.Size = new System.Drawing.Size(69, 23);
            this.bt_power_off_All_FPS3.TabIndex = 82;
            this.bt_power_off_All_FPS3.Text = "Sig all OFF";
            this.bt_power_off_All_FPS3.UseVisualStyleBackColor = true;
            this.bt_power_off_All_FPS3.Click += new System.EventHandler(this.Bt_power_off_All_FPS3_Click);
            // 
            // bt_power_off_FPS3
            // 
            this.bt_power_off_FPS3.Enabled = false;
            this.bt_power_off_FPS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bt_power_off_FPS3.Location = new System.Drawing.Point(385, 100);
            this.bt_power_off_FPS3.Name = "bt_power_off_FPS3";
            this.bt_power_off_FPS3.Size = new System.Drawing.Size(69, 23);
            this.bt_power_off_FPS3.TabIndex = 82;
            this.bt_power_off_FPS3.Text = "Signal OFF";
            this.bt_power_off_FPS3.UseVisualStyleBackColor = true;
            this.bt_power_off_FPS3.Click += new System.EventHandler(this.bt_power_off_FPS3_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(153, 26);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(47, 13);
            this.label46.TabIndex = 81;
            this.label46.Text = "Литера:";
            // 
            // cb_literaK_fps3
            // 
            this.cb_literaK_fps3.FormattingEnabled = true;
            this.cb_literaK_fps3.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cb_literaK_fps3.Location = new System.Drawing.Point(658, 86);
            this.cb_literaK_fps3.Name = "cb_literaK_fps3";
            this.cb_literaK_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_literaK_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_literaK_fps3.TabIndex = 80;
            this.cb_literaK_fps3.Text = "1";
            this.cb_literaK_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_litera_fps3_SelectedIndexChanged);
            // 
            // cb_litera_fps3
            // 
            this.cb_litera_fps3.FormattingEnabled = true;
            this.cb_litera_fps3.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cb_litera_fps3.Location = new System.Drawing.Point(216, 20);
            this.cb_litera_fps3.Name = "cb_litera_fps3";
            this.cb_litera_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_litera_fps3.Size = new System.Drawing.Size(136, 21);
            this.cb_litera_fps3.TabIndex = 80;
            this.cb_litera_fps3.Text = "1";
            this.cb_litera_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_litera_fps3_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1061, 387);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "hex";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1061, 387);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Настройки";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxStopSend);
            this.groupBox1.Controls.Add(this.checkBox_test_HEX);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.bt_open_hex);
            this.groupBox1.Controls.Add(this.checkBox_color);
            this.groupBox1.Controls.Add(this.textBox_buf_delay);
            this.groupBox1.Controls.Add(this.checkBox_visibleTxRx);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button_buffer_delay);
            this.groupBox1.Controls.Add(this.button_update_ports);
            this.groupBox1.Controls.Add(this.comboBox_portname);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.button_Close);
            this.groupBox1.Controls.Add(this.button_Open);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBox_baudrate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBox_parity);
            this.groupBox1.Controls.Add(this.comboBox_stop_bits);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(792, 256);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // checkBoxStopSend
            // 
            this.checkBoxStopSend.AutoSize = true;
            this.checkBoxStopSend.Location = new System.Drawing.Point(638, 174);
            this.checkBoxStopSend.Name = "checkBoxStopSend";
            this.checkBoxStopSend.Size = new System.Drawing.Size(73, 17);
            this.checkBoxStopSend.TabIndex = 53;
            this.checkBoxStopSend.Text = "StopSend";
            this.checkBoxStopSend.UseVisualStyleBackColor = true;
            // 
            // checkBox_test_HEX
            // 
            this.checkBox_test_HEX.AutoSize = true;
            this.checkBox_test_HEX.Location = new System.Drawing.Point(638, 84);
            this.checkBox_test_HEX.Name = "checkBox_test_HEX";
            this.checkBox_test_HEX.Size = new System.Drawing.Size(72, 17);
            this.checkBox_test_HEX.TabIndex = 52;
            this.checkBox_test_HEX.Text = "Test HEX";
            this.checkBox_test_HEX.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(638, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 51;
            this.button2.Text = "StopTimer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bt_open_hex
            // 
            this.bt_open_hex.Location = new System.Drawing.Point(638, 136);
            this.bt_open_hex.Name = "bt_open_hex";
            this.bt_open_hex.Size = new System.Drawing.Size(75, 21);
            this.bt_open_hex.TabIndex = 49;
            this.bt_open_hex.Text = "Test";
            this.bt_open_hex.UseVisualStyleBackColor = true;
            // 
            // checkBox_color
            // 
            this.checkBox_color.AutoSize = true;
            this.checkBox_color.Checked = true;
            this.checkBox_color.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_color.Location = new System.Drawing.Point(12, 136);
            this.checkBox_color.Name = "checkBox_color";
            this.checkBox_color.Size = new System.Drawing.Size(50, 17);
            this.checkBox_color.TabIndex = 34;
            this.checkBox_color.Text = "Color";
            this.checkBox_color.UseVisualStyleBackColor = true;
            // 
            // textBox_buf_delay
            // 
            this.textBox_buf_delay.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox_buf_delay.Location = new System.Drawing.Point(456, 66);
            this.textBox_buf_delay.Name = "textBox_buf_delay";
            this.textBox_buf_delay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox_buf_delay.Size = new System.Drawing.Size(67, 20);
            this.textBox_buf_delay.TabIndex = 41;
            this.textBox_buf_delay.Text = "20";
            this.textBox_buf_delay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkBox_visibleTxRx
            // 
            this.checkBox_visibleTxRx.AutoSize = true;
            this.checkBox_visibleTxRx.Checked = true;
            this.checkBox_visibleTxRx.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_visibleTxRx.Location = new System.Drawing.Point(12, 107);
            this.checkBox_visibleTxRx.Name = "checkBox_visibleTxRx";
            this.checkBox_visibleTxRx.Size = new System.Drawing.Size(59, 17);
            this.checkBox_visibleTxRx.TabIndex = 34;
            this.checkBox_visibleTxRx.Text = "On/Off";
            this.checkBox_visibleTxRx.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(319, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Сохранить настройки";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // button_buffer_delay
            // 
            this.button_buffer_delay.ForeColor = System.Drawing.Color.Black;
            this.button_buffer_delay.Location = new System.Drawing.Point(456, 9);
            this.button_buffer_delay.Name = "button_buffer_delay";
            this.button_buffer_delay.Size = new System.Drawing.Size(69, 50);
            this.button_buffer_delay.TabIndex = 12;
            this.button_buffer_delay.Text = "Размер\r\nбуфера [мс]";
            this.button_buffer_delay.UseVisualStyleBackColor = true;
            // 
            // button_update_ports
            // 
            this.button_update_ports.ForeColor = System.Drawing.Color.Black;
            this.button_update_ports.Location = new System.Drawing.Point(8, 15);
            this.button_update_ports.MinimumSize = new System.Drawing.Size(55, 23);
            this.button_update_ports.Name = "button_update_ports";
            this.button_update_ports.Size = new System.Drawing.Size(75, 48);
            this.button_update_ports.TabIndex = 16;
            this.button_update_ports.Text = "Обновить\r\nсписок\r\nпортов";
            this.button_update_ports.UseVisualStyleBackColor = true;
            this.button_update_ports.Click += new System.EventHandler(this.button_update_ports_Click);
            // 
            // comboBox_portname
            // 
            this.comboBox_portname.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_portname.FormattingEnabled = true;
            this.comboBox_portname.Location = new System.Drawing.Point(147, 17);
            this.comboBox_portname.Name = "comboBox_portname";
            this.comboBox_portname.Size = new System.Drawing.Size(64, 21);
            this.comboBox_portname.TabIndex = 6;
            this.comboBox_portname.Text = "COM1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Location = new System.Drawing.Point(217, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 26);
            this.label8.TabIndex = 9;
            this.label8.Text = "Стоп. \r\nбиты";
            this.label8.UseWaitCursor = true;
            // 
            // button_Close
            // 
            this.button_Close.Enabled = false;
            this.button_Close.ForeColor = System.Drawing.Color.Black;
            this.button_Close.Location = new System.Drawing.Point(389, 9);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(61, 50);
            this.button_Close.TabIndex = 15;
            this.button_Close.Text = "Закрыть\r\nпорт";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // button_Open
            // 
            this.button_Open.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Open.ForeColor = System.Drawing.Color.Black;
            this.button_Open.Location = new System.Drawing.Point(319, 9);
            this.button_Open.Name = "button_Open";
            this.button_Open.Size = new System.Drawing.Size(66, 50);
            this.button_Open.TabIndex = 15;
            this.button_Open.Text = "Открыть\r\n порт";
            this.button_Open.UseVisualStyleBackColor = true;
            this.button_Open.Click += new System.EventHandler(this.button_Open_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(103, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Порт";
            this.label5.UseWaitCursor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(217, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Четн.";
            this.label7.UseWaitCursor = true;
            // 
            // comboBox_baudrate
            // 
            this.comboBox_baudrate.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_baudrate.FormattingEnabled = true;
            this.comboBox_baudrate.Items.AddRange(new object[] {
            "1200",
            "1800",
            "2400",
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.comboBox_baudrate.Location = new System.Drawing.Point(147, 51);
            this.comboBox_baudrate.Name = "comboBox_baudrate";
            this.comboBox_baudrate.Size = new System.Drawing.Size(64, 21);
            this.comboBox_baudrate.TabIndex = 8;
            this.comboBox_baudrate.Text = "115200";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(90, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 26);
            this.label6.TabIndex = 9;
            this.label6.Text = "Скорость\r\n(бит/с)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.UseWaitCursor = true;
            // 
            // comboBox_parity
            // 
            this.comboBox_parity.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_parity.FormattingEnabled = true;
            this.comboBox_parity.Items.AddRange(new object[] {
            "Чет",
            "Нечет",
            "Нет",
            "Маркер",
            "Пробел"});
            this.comboBox_parity.Location = new System.Drawing.Point(257, 18);
            this.comboBox_parity.Name = "comboBox_parity";
            this.comboBox_parity.Size = new System.Drawing.Size(50, 21);
            this.comboBox_parity.TabIndex = 8;
            this.comboBox_parity.Text = "Нет";
            // 
            // comboBox_stop_bits
            // 
            this.comboBox_stop_bits.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_stop_bits.FormattingEnabled = true;
            this.comboBox_stop_bits.Items.AddRange(new object[] {
            "None",
            "1",
            "1.5",
            "2"});
            this.comboBox_stop_bits.Location = new System.Drawing.Point(257, 52);
            this.comboBox_stop_bits.Name = "comboBox_stop_bits";
            this.comboBox_stop_bits.Size = new System.Drawing.Size(50, 21);
            this.comboBox_stop_bits.TabIndex = 8;
            this.comboBox_stop_bits.Text = "1";
            // 
            // checkBoxService
            // 
            this.checkBoxService.AutoSize = true;
            this.checkBoxService.Location = new System.Drawing.Point(275, 13);
            this.checkBoxService.Name = "checkBoxService";
            this.checkBoxService.Size = new System.Drawing.Size(81, 17);
            this.checkBoxService.TabIndex = 69;
            this.checkBoxService.Text = "Service УУ";
            this.checkBoxService.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(462, 15);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(123, 12);
            this.progressBar1.TabIndex = 46;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(386, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 47;
            this.label15.Text = "Status";
            // 
            // richTextBoxOut
            // 
            this.richTextBoxOut.BackColor = System.Drawing.Color.Beige;
            this.richTextBoxOut.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBoxOut.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxOut.Location = new System.Drawing.Point(0, 485);
            this.richTextBoxOut.Name = "richTextBoxOut";
            this.richTextBoxOut.Size = new System.Drawing.Size(1069, 136);
            this.richTextBoxOut.TabIndex = 50;
            this.richTextBoxOut.Text = "";
            this.richTextBoxOut.TextChanged += new System.EventHandler(this.richTextBoxOut_TextChanged);
            // 
            // button_terminal
            // 
            this.button_terminal.Location = new System.Drawing.Point(631, 14);
            this.button_terminal.Name = "button_terminal";
            this.button_terminal.Size = new System.Drawing.Size(68, 21);
            this.button_terminal.TabIndex = 12;
            this.button_terminal.Text = "Терминал";
            this.button_terminal.UseVisualStyleBackColor = true;
            this.button_terminal.Click += new System.EventHandler(this.button_terminal_Click);
            // 
            // button_terminal_clear
            // 
            this.button_terminal_clear.Location = new System.Drawing.Point(726, 14);
            this.button_terminal_clear.Name = "button_terminal_clear";
            this.button_terminal_clear.Size = new System.Drawing.Size(68, 21);
            this.button_terminal_clear.TabIndex = 12;
            this.button_terminal_clear.Text = "Очистить";
            this.button_terminal_clear.UseVisualStyleBackColor = true;
            this.button_terminal_clear.Click += new System.EventHandler(this.button_terminal_clear_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // openFileDialogHex
            // 
            this.openFileDialogHex.FileName = "openFileDialogHex";
            // 
            // timerWaitStOK
            // 
            this.timerWaitStOK.Interval = 1000;
            this.timerWaitStOK.Tick += new System.EventHandler(this.timerWaitStOK_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_terminal_clear);
            this.panel1.Controls.Add(this.button_terminal);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.checkBoxService);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1069, 48);
            this.panel1.TabIndex = 51;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1069, 24);
            this.menuStrip1.TabIndex = 52;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(90, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // Main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 643);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.richTextBoxOut);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(200, 200);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main_form";
            this.Text = "Терминал устройства управления v 1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_form_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox_params.ResumeLayout(false);
            this.groupBox_params.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox[]  tb_freq_fps1_ind;
        private System.Windows.Forms.ComboBox[] cb_mod_fps1_ind;
        private System.Windows.Forms.ComboBox[] cb_dev_fps1_ind;
        private System.Windows.Forms.ComboBox[] cb_man_fps1_ind;
        private System.Windows.Forms.ComboBox[] cb_tim_fps1_ind;

        private System.Windows.Forms.TextBox[] tb_freq_fps2_ind;
        private System.Windows.Forms.ComboBox[] cb_mod_fps2_ind;
        private System.Windows.Forms.TextBox[] tb_dev_fps2_ind;
        private System.Windows.Forms.ComboBox[] cb_man_fps2_ind;
        private System.Windows.Forms.TextBox[] tb_man_fps2_ind;
        private System.Windows.Forms.ComboBox[] cb_tim_fps2_ind;

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_err;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox_params;
        private System.Windows.Forms.ComboBox comboBox_litera_FPS1;
        private System.Windows.Forms.ComboBox cb_tim4_fps1;
        private System.Windows.Forms.ComboBox cb_tim3_fps1;
        private System.Windows.Forms.ComboBox cb_tim2_fps1;
        private System.Windows.Forms.ComboBox cb_man4_fps1;
        private System.Windows.Forms.ComboBox cb_dev4_fps1;
        private System.Windows.Forms.ComboBox cb_mod4_fps1;
        private System.Windows.Forms.ComboBox cb_man3_fps1;
        private System.Windows.Forms.ComboBox cb_dev3_fps1;
        private System.Windows.Forms.ComboBox cb_mod3_fps1;
        private System.Windows.Forms.ComboBox cb_man2_fps1;
        private System.Windows.Forms.ComboBox cb_dev2_fps1;
        private System.Windows.Forms.ComboBox cb_mod2_fps1;
        private System.Windows.Forms.ComboBox cb_man1_fps1;
        private System.Windows.Forms.ComboBox cb_dev1_fps1;
        private System.Windows.Forms.ComboBox cb_mod1_fps1;
        private System.Windows.Forms.ComboBox cb_tim1_fps1;
        private System.Windows.Forms.ComboBox comboBox_freq_num_fps1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_duration_code;
        private System.Windows.Forms.TextBox tb_freq4_fps1;
        private System.Windows.Forms.Label label_code_freq;
        private System.Windows.Forms.TextBox tb_freq3_fps1;
        private System.Windows.Forms.TextBox tb_freq2_fps1;
        private System.Windows.Forms.Label label_freq_num;
        private System.Windows.Forms.TextBox tb_freq1_fps1;
        private System.Windows.Forms.Label label_nomer_pod;
        private System.Windows.Forms.Label label_band_code;
        private System.Windows.Forms.Button button_power_off_FPS1;
        private System.Windows.Forms.Button button_param_FPS1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox cb_tim2_fps2;
        private System.Windows.Forms.ComboBox cb_man2_fps2;
        private System.Windows.Forms.ComboBox cb_mod2_fps2;
        private System.Windows.Forms.ComboBox cb_man1_fps2;
        private System.Windows.Forms.ComboBox cb_mod1_fps2;
        private System.Windows.Forms.ComboBox cb_tim1_fps2;
        private System.Windows.Forms.ComboBox comboBox_freq_num_fps2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_freq2_fps2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_freq1_fps2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button_param_FPS2;
        private System.Windows.Forms.Button button_power_off_FPS2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_litera_FPS2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_buf_delay;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_buffer_delay;
        private System.Windows.Forms.Button button_update_ports;
        private System.Windows.Forms.ComboBox comboBox_portname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Button button_Open;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox_baudrate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_parity;
        private System.Windows.Forms.ComboBox comboBox_stop_bits;
        private System.Windows.Forms.Button button_terminal;
        private System.Windows.Forms.Button button_terminal_clear;
        private System.Windows.Forms.CheckBox checkBox_color;
        private System.Windows.Forms.CheckBox checkBox_visibleTxRx;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox tb_dev1_fps2;
        private System.Windows.Forms.TextBox tb_man2_fps2;
        private System.Windows.Forms.TextBox tb_man1_fps2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox checkBox_GPS_L2;
        private System.Windows.Forms.CheckBox checkBox_GPS_L1;
        private System.Windows.Forms.CheckBox checkBox_GLONASS_L2;
        private System.Windows.Forms.CheckBox checkBox_GLONASS_L1;
        private System.Windows.Forms.TextBox tb_dev2_fps2;
        private System.Windows.Forms.Button button_power_on_FPS2;
        private System.Windows.Forms.Button button_status_on_FPS2;
        private System.Windows.Forms.Button button_power_off_all_FPS2;
        private System.Windows.Forms.Button button_Amp_FPS2;
        private System.Windows.Forms.TextBox tb_ampV_fps2;
        private System.Windows.Forms.Button button_Test_FPS2;
        private System.Windows.Forms.Button button_power2_off_FPS2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_ampN_fps2;
        private System.Windows.Forms.CheckBox checkBoxService;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button_GetAmp_FPS2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelParamK;
        private System.Windows.Forms.Button button_Get_Ver_FPS2;
        private System.Windows.Forms.Label labeVer;
        private System.Windows.Forms.Label labelGetNv;
        private System.Windows.Forms.Label labelGetNn;
        private System.Windows.Forms.ListBox listBoxErr;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button bt_open_hex;
        private System.Windows.Forms.OpenFileDialog openFileDialogHex;
        private System.Windows.Forms.RichTextBox richTextBoxOut;
        private System.Windows.Forms.Timer timerWaitStOK;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.CheckBox checkBox_test_HEX;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckBox checkBoxStopSend;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ListBox listBoxErr_fps3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button bt_Get_Ver_FPS2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button bt_GetAmp_fps3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tb_ampNn_fps3;
        private System.Windows.Forms.TextBox tb_ampNv_fps3;
        private System.Windows.Forms.Button bt_Amp_fps3;
        private System.Windows.Forms.Button bt_get_status_fps3;
        private System.Windows.Forms.CheckBox checkBox_GPS_L2_fps3;
        private System.Windows.Forms.CheckBox checkBox_GPS_L1_fps3;
        private System.Windows.Forms.CheckBox checkBox_GL_L2_fps3;
        private System.Windows.Forms.CheckBox checkBox_GL_L1_fps3;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tb_dev1_fps3;
        private System.Windows.Forms.TextBox tb_man1_fps3;
        private System.Windows.Forms.ComboBox cb_man1_fps3;
        private System.Windows.Forms.ComboBox cb_mod1_fps3;
        private System.Windows.Forms.ComboBox cb_tim1_fps3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tb_freq1_fps3;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button bt_set_param_FPS3;
        private System.Windows.Forms.Button bt_power_off_FPS3;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cb_litera_fps3;
        private System.Windows.Forms.TextBox tb_freq2_fps3;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cb_literaK_fps3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button bt_power_off_All_FPS3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cb_literaCH_fps3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button saveParamAtBooster;
        private System.Windows.Forms.Button getParams;
    }
}

