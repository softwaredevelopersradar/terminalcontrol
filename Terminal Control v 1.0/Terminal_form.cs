﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Terminal_Control_v_1._1
{
    public partial class Terminal_form : Form
    {
        public Terminal_form(Form Main_form)
        {

            // Добавляем обработчик события - который запустит функцию ReloadХХХХХХ
            CallBackMy.callbackEventHandlerWt = new CallBackMy.callbackEvent(this.ReloadWhite);
            CallBackMy.callbackEventHandlerLY = new CallBackMy.callbackEvent(this.ReloadYellow);
            CallBackMy.callbackEventHandlerLR = new CallBackMy.callbackEvent(this.Reload_LRed);
            CallBackMy.callbackEventHandlerLG = new CallBackMy.callbackEvent(this.Reload_LGreen);
            CallBackMy.callbackEventHandlerLB = new CallBackMy.callbackEvent(this.Reload_LBlue);
            CallBackMy.callbackEventHandlerAq = new CallBackMy.callbackEvent(this.Reload_Aqua);
            CallBackMy.callbackEventHandlerOr = new CallBackMy.callbackEvent(this.Reload_Orange);
            CallBackMy.callbackEventHandlerPk = new CallBackMy.callbackEvent(this.Reload_Pink);
            CallBackMy.callbackEventHandlerCLC = new CallBackMy.callbackEvent(this.ReloadClear);
            InitializeComponent();
        }

        void ReloadWhite(StringBuilder param)
        {
            TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
            TextBox_Mess.SelectionBackColor = Color.White;
            TextBox_Mess.AppendText(param.ToString());
            TextBox_Mess.AppendText("\n");
            TextBox_Mess.ScrollToCaret();
        }
        void ReloadYellow(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(255, 255, 190);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void Reload_LRed(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(255, 220, 220);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void Reload_LGreen(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(210, 255, 210);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void Reload_LBlue(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(210, 210, 255);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void Reload_Aqua(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(200, 250, 250);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void Reload_Orange(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(255, 200, 150);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void Reload_Pink(StringBuilder param)
        {
                TextBox_Mess.SelectionStart = TextBox_Mess.TextLength;
                TextBox_Mess.SelectionBackColor = Color.FromArgb(255, 223, 234);
                TextBox_Mess.AppendText(param.ToString());
                TextBox_Mess.AppendText("\n");
                TextBox_Mess.ScrollToCaret();
        }
        void ReloadClear(StringBuilder param)
        {
            TextBox_Mess.Text = "";
            TextBox_Mess.ScrollToCaret();
        }

        private void Terminal_form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4)
            {
                e.Handled = true;
                //new CancelEventArgs(true);
            }
        }

        private void TextBox_Mess_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Alt & e.KeyCode == Keys.T)
            if (e.Control)
            {
                float CC = TextBox_Mess.ZoomFactor;
                //label_Visibility14.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
                if (CC > 2)
                {
                    CC = 2;
                    TextBox_Mess.ZoomFactor = 2;
                }
                else if (CC < 1)
                {
                    CC = 1;
                    TextBox_Mess.ZoomFactor = 1;
                }
                label_receiveline.Font = new Font("Courier New", 8 * CC, FontStyle.Regular);
                label_receiveline_Ver.Font = new Font("Courier New", 8 * CC, FontStyle.Regular);
            }
        }

        private void TextBox_Mess_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
