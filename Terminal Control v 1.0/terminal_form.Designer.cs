﻿namespace Terminal_Control_v_1._1
{
    partial class Terminal_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox_Mess = new System.Windows.Forms.RichTextBox();
            this.label_receiveline_Ver = new System.Windows.Forms.Label();
            this.label_receiveline = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBox_Mess
            // 
            this.TextBox_Mess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextBox_Mess.Font = new System.Drawing.Font("Courier New", 8.25F);
            this.TextBox_Mess.Location = new System.Drawing.Point(37, 33);
            this.TextBox_Mess.Name = "TextBox_Mess";
            this.TextBox_Mess.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.TextBox_Mess.Size = new System.Drawing.Size(621, 259);
            this.TextBox_Mess.TabIndex = 43;
            this.TextBox_Mess.Text = "";
            this.TextBox_Mess.TextChanged += new System.EventHandler(this.TextBox_Mess_TextChanged);
            this.TextBox_Mess.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_Mess_KeyDown);
            // 
            // label_receiveline_Ver
            // 
            this.label_receiveline_Ver.AutoSize = true;
            this.label_receiveline_Ver.Font = new System.Drawing.Font("Courier New", 8.25F);
            this.label_receiveline_Ver.Location = new System.Drawing.Point(3, 33);
            this.label_receiveline_Ver.Name = "label_receiveline_Ver";
            this.label_receiveline_Ver.Size = new System.Drawing.Size(21, 700);
            this.label_receiveline_Ver.TabIndex = 44;
            this.label_receiveline_Ver.Text = "01\r\n02\r\n03\r\n04\r\n05\r\n06\r\n07\r\n08\r\n09\r\n10\r\n11\r\n12\r\n13\r\n14\r\n15\r\n16\r\n17\r\n18\r\n19\r\n20\r\n2" +
    "1\r\n22\r\n23\r\n24\r\n25\r\n26\r\n27\r\n28\r\n29\r\n30\r\n31\r\n32\r\n33\r\n34\r\n35\r\n36\r\n37\r\n38\r\n39\r\n40\r\n4" +
    "1\r\n42\r\n43\r\n44\r\n45\r\n46\r\n47\r\n48\r\n49\r\n50";
            // 
            // label_receiveline
            // 
            this.label_receiveline.AutoSize = true;
            this.label_receiveline.Font = new System.Drawing.Font("Courier New", 8.25F);
            this.label_receiveline.Location = new System.Drawing.Point(37, 7);
            this.label_receiveline.Name = "label_receiveline";
            this.label_receiveline.Size = new System.Drawing.Size(840, 14);
            this.label_receiveline.TabIndex = 45;
            this.label_receiveline.Text = "01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 " +
    "28 29 30 31 32 33 34 35 36 37 38 39 40";
            // 
            // Terminal_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 304);
            this.ControlBox = false;
            this.Controls.Add(this.TextBox_Mess);
            this.Controls.Add(this.label_receiveline_Ver);
            this.Controls.Add(this.label_receiveline);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(150, 500);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(911, 770);
            this.Name = "Terminal_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Терминал";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Terminal_form_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox TextBox_Mess;
        private System.Windows.Forms.Label label_receiveline_Ver;
        private System.Windows.Forms.Label label_receiveline;
    }
}