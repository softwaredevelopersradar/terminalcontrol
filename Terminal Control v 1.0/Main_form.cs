﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Xml.Serialization;

namespace Terminal_Control_v_1._1
{
    enum DATA_COMAND : byte
    {
        /* Application's state machine's initial state. */
        NO_CMD = 0x00,
        READ_BOOT_INFO = 0x01,
        PROGRAM_FLASH = 0x81,
        ERASE_FLASH = 0x82,
        READ_FLASH = 0x83,
        JUMP_App = 0x84,
        SET_segADDR = 0x85
    }
    enum HEX_EVENT
    { // вынесено для удобства из SOURCE_EVENT
        USER_HEX_dsPIC33_EVENT,// загрузка hex для dsPIC 
        USER_HEX_PIC32_EVENT// загрузка hex для PIC32			
    }

    enum SOURCE_EVENT
    {
        /* Application's state machine's initial state. */
        no_EVENT,
        USER_EVENT, // событие от пользователя
        TIMER_EVENT,// событие по таймеру
        SP_EVENT,// событие с приемника последовательного порта
        USER_ERASE_EVENT// команда очистки памяти
    }

    public struct HEX_RECORD_DEF
    {
        public int addressMCU;
        public uint DATA;
        public bool file_end;
        public int fileLinesCount;
        private DATA_COMAND laststatusCMD;// предыдущее состояние выполнения команды
        private DATA_COMAND statusCMD;// текущее состояние выполнения команды
        private SOURCE_EVENT srcEvent; // здесь храним данные события: таймера, порта
        private HEX_EVENT hexEvent; // от кого событие

        public uint segmentAddress; // последний адрес записи блока данных программы
        public uint linearAddress;// последовательный адрес
        public uint setAddress;
        public int t_fileLinesCount;// резервируем номер строки
        public uint t_segmentAddress; // последний адрес записи блока данных программы
        public uint t_linearAddress;// последовательный адрес
        public uint t_setAddress;


        internal HEX_EVENT hexEVNT
        {
            get
            {
                return hexEvent;
            }

            set
            {
                hexEvent = value;
            }
        }

        internal SOURCE_EVENT EVNT
        {
            get
            {
                return srcEvent;
            }

            set
            {
                srcEvent = value;
            }
        }

        internal DATA_COMAND CMD
        {
            get
            {
                return statusCMD;
            }

            set
            {
                statusCMD = value;
            }
        }
        internal DATA_COMAND LASTCMD
        {
            get
            {
                return laststatusCMD;
            }

            set
            {
                laststatusCMD = value;
            }
        }
    }
    /*
        enum HEX_EVENT_def
        {
            // Application's state machine's initial state. 
            PIC32_EVENT,
            dsPIC33_EVENT
        }
*/


    public partial class Main_form : Form
    {

        Terminal_form TermForm;
        string Openfilename;
        List<string> fileLines = new List<string>();
        uint segmentAddress; // последний адрес записи блока данных программы
        uint setAddress; // при успешной отправке увеличивает segmentAddress на setAddress
        uint cnt_check_data; // счатчик ожидания ответа
        uint setAddress_page; // адрес для 020000040002F8

        int baudrate;
        string portName;
        int curIndex;

        string firstFr;
        string secondFr;
        string modulation;
        string gain;

        bool gPS_L1_fps3IsChecked;
        bool gPS_L2_fps3IsChecked;
        bool gL_L1_fps3IsEChecked;
        bool gL_L2_fps3IsEChecked;

        bool flgEnd = false;

        int curRow;
        bool idIsTaken = false;


        HEX_RECORD_DEF HEX_RECORD = new HEX_RECORD_DEF();
        BindingList<Info> data = new BindingList<Info>();
        LCHMParams curLCHMParams;

        // HEX_EVENT_def HEX_EVENT;
        // вот этот самый класс, который мы будем сохранять
        public class iniSettings // имя выбрано просто для читаемости кода впоследствии
        {
            //public int num_ports; // это собственно для хранения X верхнего левого угла окна
            public string portName;
            public int baudrate; // ну а это, соответственно Y

            //TODO: create separated class
            public string firstFr;
            public string secondFr;
            public string modulation;
            public string gain;
            public string litera;

            public bool GPS_L1_fps3IsChecked;
            public bool GPS_L2_fps3IsChecked;
            public bool GL_L1_fps3IsChecked;
            public bool GL_L2_fps3IsChecked;

            public BindingList<Info> latestData;
        }

        public class Info
        {
            public uint Id { get; set; }
            public uint Fr1 { get; set; }
            public uint Fr2 { get; set; }
            public uint K1 { get; set; }
            public uint K2 { get; set; }
            public uint Status { get; set; }

        }

        public class LCHMParams
        {
            public string Deviation { get; set; }
            public string TScan { get; set; }
            public string TCode { get; set; }
        }

        public Main_form()
        {
            curLCHMParams = new LCHMParams();
            InitializeComponent();
            getAvailablePorts();
            LoadSettings();
            SetPortBaudrate();
            FillDataGreed();
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            getParams.Enabled = true;
            saveParamAtBooster.Enabled = true;

            dataGridView1.CellValidating += new DataGridViewCellValidatingEventHandler(dataGridView1_CellValidating);
            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            dataGridView1.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(dataGridView1_HeaderClick);

            dataGridView1.CellEndEdit += new DataGridViewCellEventHandler(dataGridView1_IDValidating);

            TermForm = new Terminal_form(this);

            cnt_check_data = 10;
            segmentAddress = 0;
            setAddress_page = 0;
            setAddress = 0;
            tb_freq_fps1_ind = new TextBox[4];
            tb_freq_fps1_ind[0] = tb_freq1_fps1;
            tb_freq_fps1_ind[1] = tb_freq2_fps1;
            tb_freq_fps1_ind[2] = tb_freq3_fps1;
            tb_freq_fps1_ind[3] = tb_freq4_fps1;

            cb_mod_fps1_ind = new ComboBox[4];
            cb_mod_fps1_ind[0] = cb_mod1_fps1;
            cb_mod_fps1_ind[1] = cb_mod2_fps1;
            cb_mod_fps1_ind[2] = cb_mod3_fps1;
            cb_mod_fps1_ind[3] = cb_mod4_fps1;

            cb_dev_fps1_ind = new ComboBox[4];
            cb_dev_fps1_ind[0] = cb_dev1_fps1;
            cb_dev_fps1_ind[1] = cb_dev2_fps1;
            cb_dev_fps1_ind[2] = cb_dev3_fps1;
            cb_dev_fps1_ind[3] = cb_dev4_fps1;

            cb_man_fps1_ind = new ComboBox[4];
            cb_man_fps1_ind[0] = cb_man1_fps1;
            cb_man_fps1_ind[1] = cb_man2_fps1;
            cb_man_fps1_ind[2] = cb_man3_fps1;
            cb_man_fps1_ind[3] = cb_man4_fps1;

            cb_tim_fps1_ind = new ComboBox[4];
            cb_tim_fps1_ind[0] = cb_tim1_fps1;
            cb_tim_fps1_ind[1] = cb_tim2_fps1;
            cb_tim_fps1_ind[2] = cb_tim3_fps1;
            cb_tim_fps1_ind[3] = cb_tim4_fps1;

            //------------------------------------------

            tb_freq_fps2_ind = new TextBox[4];
            tb_freq_fps2_ind[0] = tb_freq1_fps2;
            tb_freq_fps2_ind[1] = tb_freq2_fps2;


            cb_mod_fps2_ind = new ComboBox[4];
            cb_mod_fps2_ind[0] = cb_mod1_fps2;
            cb_mod_fps2_ind[1] = cb_mod2_fps2;


            tb_dev_fps2_ind = new TextBox[4];
            tb_dev_fps2_ind[0] = tb_dev1_fps2;
            tb_dev_fps2_ind[1] = tb_dev2_fps2;


            cb_man_fps2_ind = new ComboBox[4];
            cb_man_fps2_ind[0] = cb_man1_fps2;
            cb_man_fps2_ind[1] = cb_man2_fps2;

            tb_man_fps2_ind = new TextBox[4];
            tb_man_fps2_ind[0] = tb_man1_fps2;
            tb_man_fps2_ind[1] = tb_man2_fps2;


            cb_tim_fps2_ind = new ComboBox[4];
            cb_tim_fps2_ind[0] = cb_tim1_fps2;
            cb_tim_fps2_ind[1] = cb_tim2_fps2;

            tb_freq2_fps3.Enabled = true;

        }


        private void LoadSettings()
        {
            LoadCPSettings();
            SetPortName();
            SetPortBaudrate();
            SetFrAndGain();
            SetModulation();
            SetCheckers();

        }


        private void SetPortName()
        {
            for (int i = 0; i < comboBox_portname.Items.Count; i++)
            {
                comboBox_portname.SelectedIndex = i;
                if (comboBox_portname.Text == portName)
                    return;
            }
        }

        private void SetPortBaudrate()
        {
            for (int i = 0; i < comboBox_baudrate.Items.Count; i++)
            {
                comboBox_baudrate.SelectedIndex = i;
                if (comboBox_baudrate.Text == baudrate.ToString())
                    return;
            }
        }

        private void SetModulation()
        {
            if (modulation != null)
            {
                for (int i = 0; i < cb_mod1_fps3.Items.Count; i++)
                {
                    cb_mod1_fps3.SelectedIndex = i;
                    if (cb_mod1_fps3.Text == modulation.ToString())
                        return;
                }
            }
        }

        private void SetFrAndGain()
        {
            if (firstFr != null)
                tb_freq1_fps3.Text = firstFr;
            if (secondFr != null)
                tb_freq2_fps3.Text = secondFr;
            if (gain != null)
                label32.Text = gain;
        }

        private void SetCheckers()
        {
            checkBox_GPS_L1_fps3.Checked = gPS_L1_fps3IsChecked;
            checkBox_GPS_L2_fps3.Checked = gPS_L2_fps3IsChecked;
            checkBox_GL_L1_fps3.Checked = gL_L1_fps3IsEChecked;
            checkBox_GL_L2_fps3.Checked = gL_L2_fps3IsEChecked;
        }



        void getAvailablePorts()
        {
            string[] ports = SerialPort.GetPortNames();
            button_update_ports.Enabled = false;
            comboBox_portname.Items.Clear();
            comboBox_portname.Items.AddRange(ports);
            if (comboBox_portname.Items.Count > 0)
            {
                comboBox_portname.SelectedIndex = 0;
            }
            else
            {
                comboBox_portname.Text = "----";
            }
            button_update_ports.Enabled = true;
        }

        private void button_update_ports_Click(object sender, EventArgs e)
        {
            getAvailablePorts();
        }

        private void button_Open_Click(object sender, EventArgs e)
        {
            try
            {

                if (comboBox_portname.Text == "" || comboBox_baudrate.Text == "")
                {

                    MessageBox.Show("Ошибка! Не выбран порт", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    serialPort1.PortName = comboBox_portname.Text;
                    serialPort1.BaudRate = Convert.ToInt32(comboBox_baudrate.Text);
                    button_Close.Enabled = true;
                    button_power_off_FPS2.Enabled = true;
                    button_param_FPS2.Enabled = true;
                    button_power_off_FPS1.Enabled = true;
                    button_param_FPS1.Enabled = true;
                    button_power_on_FPS2.Enabled = true;
                    button_status_on_FPS2.Enabled = true;
                    button_Amp_FPS2.Enabled = true;
                    button_GetAmp_FPS2.Enabled = true;
                    button_Test_FPS2.Enabled = true;
                    button_power2_off_FPS2.Enabled = true;
                    button_power_off_all_FPS2.Enabled = true;
                    bt_power_off_All_FPS3.Enabled = true;
                    button_Get_Ver_FPS2.Enabled = true;
                    button_Open.Enabled = false;
                    comboBox_portname.Enabled = false;
                    comboBox_baudrate.Enabled = false;
                    comboBox_stop_bits.Enabled = false;
                    comboBox_parity.Enabled = false;

                    bt_set_param_FPS3.Enabled = true;
                    bt_power_off_FPS3.Enabled = true;
                    bt_get_status_fps3.Enabled = true;
                    bt_Amp_fps3.Enabled = true;
                    bt_GetAmp_fps3.Enabled = true;
                    //checkBox_GPS_L1_fps3.Enabled = true;
                    //checkBox_GPS_L2_fps3.Enabled = true;
                    //checkBox_GL_L1_fps3.Enabled = true;
                    //checkBox_GL_L2_fps3.Enabled = true;

                    tb_freq2_fps3.Enabled = true;
                    bt_Get_Ver_FPS2.Enabled = true;
                    button3.Enabled = true;

                    switch (comboBox_parity.SelectedIndex)
                    {
                        case 0:
                            serialPort1.Parity = Parity.Even;
                            break;
                        case 1:
                            serialPort1.Parity = Parity.Mark;
                            break;
                        case 2:
                            serialPort1.Parity = Parity.None;
                            break;
                        case 3:
                            serialPort1.Parity = Parity.Odd;
                            break;
                        case 4:
                            serialPort1.Parity = Parity.Space;
                            break;
                        default:
                            serialPort1.Parity = Parity.None;
                            break;
                    }
                    serialPort1.DataBits = 8;
                    switch (comboBox_stop_bits.SelectedIndex)
                    {
                        case 0:
                            serialPort1.StopBits = StopBits.None;
                            break;
                        case 1:
                            serialPort1.StopBits = StopBits.One;
                            break;
                        case 2:
                            serialPort1.StopBits = StopBits.OnePointFive;
                            break;
                        case 3:
                            serialPort1.StopBits = StopBits.Two;
                            break;
                        default:
                            serialPort1.StopBits = StopBits.One;
                            break;
                    }
                    serialPort1.RtsEnable = true;
                    serialPort1.DtrEnable = false;
                    serialPort1.ReadTimeout = 100;
                    //serialPort1.WriteTimeout = 10;
                    serialPort1.WriteBufferSize = 2048;
                    serialPort1.ReadBufferSize = 2048;

                    serialPort1.Open();
                    progressBar1.Step = 1;
                    progressBar1.Maximum = 100;

                    progressBar1.Value = 100;
                    toolStripStatusLabel1.Text = "Port = \"" + serialPort1.PortName + "\", BaudRate = " + serialPort1.BaudRate.ToString();

                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка подключения!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            }
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            if (serialPort1.IsOpen == false)
            {
                toolStripStatusLabel1.Text = "Port not initialized";
                progressBar1.Value = 0;
                button_Close.Enabled = false;
                button_power_off_FPS2.Enabled = false;
                button_param_FPS2.Enabled = false;
                button_power_off_FPS1.Enabled = false;
                button_param_FPS1.Enabled = false;
                button_power_on_FPS2.Enabled = false;
                button_status_on_FPS2.Enabled = false;
                button_Amp_FPS2.Enabled = false;
                button_GetAmp_FPS2.Enabled = false;
                button_Test_FPS2.Enabled = false;
                button_power2_off_FPS2.Enabled = false;
                button_power_off_all_FPS2.Enabled = false;
                button_Get_Ver_FPS2.Enabled = false;
                bt_power_off_All_FPS3.Enabled = false;
                button_Open.Enabled = true;
                comboBox_portname.Enabled = true;
                comboBox_baudrate.Enabled = true;
                comboBox_stop_bits.Enabled = true;
                comboBox_parity.Enabled = true;

                tb_freq2_fps3.Enabled = false;

                bt_set_param_FPS3.Enabled = false;
                bt_power_off_FPS3.Enabled = false;
                bt_get_status_fps3.Enabled = false;
                bt_Amp_fps3.Enabled = false;
                bt_GetAmp_fps3.Enabled = false;
                bt_Get_Ver_FPS2.Enabled = false;
                button3.Enabled = false;
            }

        }

        private void button_terminal_Click(object sender, EventArgs e)
        {


            if (TermForm.Visible == true)
            {
                TermForm.Visible = false;
            }
            else
            {

                TermForm.Visible = true;
            }
        }

        private void button_terminal_clear_Click(object sender, EventArgs e)
        {
            CallBackMy.callbackEventHandlerCLC(null);

            richTextBoxOut.Clear();

        }

        private void comboBox_freq_num_fps1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            switch (comboBox_freq_num_fps1.SelectedIndex)
            {
                case 0:
                    for (i = 1; i < 4; i++)
                    { // закрываем все кроме первой
                        tb_freq_fps1_ind[i].Enabled = false;
                        cb_mod_fps1_ind[i].Enabled = false;
                        cb_dev_fps1_ind[i].Enabled = false;
                        cb_man_fps1_ind[i].Enabled = false;
                        cb_tim_fps1_ind[i].Enabled = false;
                    }
                    break;
                case 1:
                    for (i = 2; i < 4; i++)
                    { // сначала  закрываем со 2 
                        tb_freq_fps1_ind[i].Enabled = false;
                        cb_mod_fps1_ind[i].Enabled = false;
                        cb_dev_fps1_ind[i].Enabled = false;
                        cb_man_fps1_ind[i].Enabled = false;
                        cb_tim_fps1_ind[i].Enabled = false;
                    }
                    for (i = 1; i < 2; i++)
                    { // потом открываем необходимые - 2
                        tb_freq_fps1_ind[i].Enabled = true;
                        cb_mod_fps1_ind[i].Enabled = true;
                        cb_dev_fps1_ind[i].Enabled = true;
                        cb_man_fps1_ind[i].Enabled = true;
                        cb_tim_fps1_ind[i].Enabled = true;
                    }
                    break;
                case 2:
                    for (i = 3; i < 4; i++)
                    {
                        tb_freq_fps1_ind[i].Enabled = false;
                        cb_mod_fps1_ind[i].Enabled = false;
                        cb_dev_fps1_ind[i].Enabled = false;
                        cb_man_fps1_ind[i].Enabled = false;
                        cb_tim_fps1_ind[i].Enabled = false;
                    }
                    for (i = 1; i < 3; i++)
                    {// открываем 3
                        tb_freq_fps1_ind[i].Enabled = true;
                        cb_mod_fps1_ind[i].Enabled = true;
                        cb_dev_fps1_ind[i].Enabled = true;
                        cb_man_fps1_ind[i].Enabled = true;
                        cb_tim_fps1_ind[i].Enabled = true;
                    }
                    break;
                case 3:
                    for (i = 1; i < 4; i++)
                    { //открываем 4
                        tb_freq_fps1_ind[i].Enabled = true;
                        cb_mod_fps1_ind[i].Enabled = true;
                        cb_dev_fps1_ind[i].Enabled = true;
                        cb_man_fps1_ind[i].Enabled = true;
                        cb_tim_fps1_ind[i].Enabled = true;
                    }
                    break;
            }
        }

        private void cb_mod1_fps1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod1_fps1.SelectedIndex)
            {
                case 0:
                    cb_dev1_fps1.Items.Clear();
                    cb_dev1_fps1.Items.Add("---");
                    cb_man1_fps1.Items.Clear();
                    cb_man1_fps1.Items.Add("---");
                    cb_dev1_fps1.Text = "---";
                    cb_man1_fps1.Text = "---";
                    break;
                case 1:
                    cb_dev1_fps1.Items.Clear();
                    cb_dev1_fps1.Items.Add("---");
                    cb_dev1_fps1.Items.Add("±1,75");
                    cb_dev1_fps1.Items.Add("±2,47");
                    cb_dev1_fps1.Items.Add("±3,5");
                    cb_dev1_fps1.Items.Add("±4,2");
                    cb_dev1_fps1.Items.Add("±5,0");
                    cb_dev1_fps1.Items.Add("±7,07");
                    cb_dev1_fps1.Items.Add("±10,0");
                    cb_dev1_fps1.Items.Add("±15,8");
                    cb_dev1_fps1.Items.Add("±25,0");
                    cb_dev1_fps1.Items.Add("±35,35");
                    cb_dev1_fps1.Items.Add("±50,0");
                    cb_dev1_fps1.Items.Add("±70,0");
                    cb_dev1_fps1.Items.Add("±100,0");
                    cb_dev1_fps1.Items.Add("±150,0");
                    cb_dev1_fps1.Items.Add("±250,0");
                    cb_dev1_fps1.Items.Add("±350,0");
                    cb_dev1_fps1.Items.Add("±500,0");
                    cb_dev1_fps1.Items.Add("±700,0");
                    cb_dev1_fps1.Items.Add("±1000");
                    cb_dev1_fps1.Text = "±1,75";
                    //
                    cb_man1_fps1.Items.Clear();
                    cb_man1_fps1.Items.Add("---");
                    cb_man1_fps1.Text = "---";
                    break;
                case 2:
                case 3:
                case 4:
                    cb_dev1_fps1.Items.Clear();
                    cb_dev1_fps1.Items.Add("---");
                    cb_dev1_fps1.Items.Add("±2,0");
                    cb_dev1_fps1.Items.Add("±4,0");
                    cb_dev1_fps1.Items.Add("±6,0");
                    cb_dev1_fps1.Items.Add("±8,0");
                    cb_dev1_fps1.Items.Add("±10,0");
                    cb_dev1_fps1.Items.Add("±12,0");
                    cb_dev1_fps1.Items.Add("±14,0");
                    cb_dev1_fps1.Items.Add("±16,0");
                    cb_dev1_fps1.Items.Add("±18,0");
                    cb_dev1_fps1.Items.Add("±20,0");
                    cb_dev1_fps1.Items.Add("±22,0");
                    cb_dev1_fps1.Items.Add("±24,0");
                    cb_dev1_fps1.Text = "±2,0";
                    //
                    cb_man1_fps1.Items.Clear();
                    cb_man1_fps1.Items.Add("---");
                    cb_man1_fps1.Items.Add("62,5");
                    cb_man1_fps1.Items.Add("125");
                    cb_man1_fps1.Items.Add("250");
                    cb_man1_fps1.Items.Add("500");
                    cb_man1_fps1.Items.Add("1000");
                    cb_man1_fps1.Items.Add("2000");
                    cb_man1_fps1.Text = "62,5";
                    break;
                case 5:
                case 6:
                case 7:
                    //
                    cb_dev1_fps1.Items.Clear();
                    cb_dev1_fps1.Items.Add("---");
                    cb_dev1_fps1.Text = "---";
                    //
                    cb_man1_fps1.Items.Clear();
                    cb_man1_fps1.Items.Add("---");
                    cb_man1_fps1.Items.Add("0,2");
                    cb_man1_fps1.Items.Add("0,5");
                    cb_man1_fps1.Items.Add("1,0");
                    cb_man1_fps1.Items.Add("5,0");
                    cb_man1_fps1.Items.Add("10,0");
                    cb_man1_fps1.Items.Add("20,0");
                    cb_man1_fps1.Items.Add("40,0");
                    cb_man1_fps1.Items.Add("80,0");
                    cb_man1_fps1.Items.Add("400,0");
                    cb_man1_fps1.Text = "0,2";
                    break;
                case 8:
                    cb_dev1_fps1.Items.Clear();
                    cb_dev1_fps1.Items.Add("---");
                    cb_dev1_fps1.Text = "---";
                    //
                    cb_man1_fps1.Items.Clear();
                    cb_man1_fps1.Items.Add("---");
                    cb_man1_fps1.Items.Add("0,2");
                    cb_man1_fps1.Text = "0,2";
                    break;
            }
        }

        private void cb_mod2_fps1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod2_fps1.SelectedIndex)
            {
                case 0:
                    cb_dev2_fps1.Items.Clear();
                    cb_dev2_fps1.Items.Add("---");
                    cb_man2_fps1.Items.Clear();
                    cb_man2_fps1.Items.Add("---");
                    cb_dev2_fps1.Text = "---";
                    cb_man2_fps1.Text = "---";
                    break;
                case 1:
                    cb_dev2_fps1.Items.Clear();
                    cb_dev2_fps1.Items.Add("---");
                    cb_dev2_fps1.Items.Add("±1,75");
                    cb_dev2_fps1.Items.Add("±2,47");
                    cb_dev2_fps1.Items.Add("±3,5");
                    cb_dev2_fps1.Items.Add("±4,2");
                    cb_dev2_fps1.Items.Add("±5,0");
                    cb_dev2_fps1.Items.Add("±7,07");
                    cb_dev2_fps1.Items.Add("±10,0");
                    cb_dev2_fps1.Items.Add("±15,8");
                    cb_dev2_fps1.Items.Add("±25,0");
                    cb_dev2_fps1.Items.Add("±35,35");
                    cb_dev2_fps1.Items.Add("±50,0");
                    cb_dev2_fps1.Items.Add("±70,0");
                    cb_dev2_fps1.Items.Add("±100,0");
                    cb_dev2_fps1.Items.Add("±150,0");
                    cb_dev2_fps1.Items.Add("±250,0");
                    cb_dev2_fps1.Items.Add("±350,0");
                    cb_dev2_fps1.Items.Add("±500,0");
                    cb_dev2_fps1.Items.Add("±700,0");
                    cb_dev2_fps1.Items.Add("±1000");
                    cb_dev2_fps1.Text = "±1,75";
                    //
                    cb_man2_fps1.Items.Clear();
                    cb_man2_fps1.Items.Add("---");
                    cb_man2_fps1.Text = "---";
                    break;
                case 2:
                case 3:
                case 4:
                    cb_dev2_fps1.Items.Clear();
                    cb_dev2_fps1.Items.Add("---");
                    cb_dev2_fps1.Items.Add("±2,0");
                    cb_dev2_fps1.Items.Add("±4,0");
                    cb_dev2_fps1.Items.Add("±6,0");
                    cb_dev2_fps1.Items.Add("±8,0");
                    cb_dev2_fps1.Items.Add("±10,0");
                    cb_dev2_fps1.Items.Add("±12,0");
                    cb_dev2_fps1.Items.Add("±14,0");
                    cb_dev2_fps1.Items.Add("±16,0");
                    cb_dev2_fps1.Items.Add("±18,0");
                    cb_dev2_fps1.Items.Add("±20,0");
                    cb_dev2_fps1.Items.Add("±22,0");
                    cb_dev2_fps1.Items.Add("±24,0");
                    cb_dev2_fps1.Text = "±2,0";
                    //
                    cb_man2_fps1.Items.Clear();
                    cb_man2_fps1.Items.Add("---");
                    cb_man2_fps1.Items.Add("62,5");
                    cb_man2_fps1.Items.Add("125");
                    cb_man2_fps1.Items.Add("250");
                    cb_man2_fps1.Items.Add("500");
                    cb_man2_fps1.Items.Add("1000");
                    cb_man2_fps1.Items.Add("2000");
                    cb_man2_fps1.Text = "62,5";
                    break;
                case 5:
                case 6:
                case 7:
                    //
                    cb_dev2_fps1.Items.Clear();
                    cb_dev2_fps1.Items.Add("---");
                    cb_dev2_fps1.Text = "---";
                    //
                    cb_man2_fps1.Items.Clear();
                    cb_man2_fps1.Items.Add("---");
                    cb_man2_fps1.Items.Add("0,2");
                    cb_man2_fps1.Items.Add("0,5");
                    cb_man2_fps1.Items.Add("1,0");
                    cb_man2_fps1.Items.Add("5,0");
                    cb_man2_fps1.Items.Add("10,0");
                    cb_man2_fps1.Items.Add("20,0");
                    cb_man2_fps1.Items.Add("40,0");
                    cb_man2_fps1.Items.Add("80,0");
                    cb_man2_fps1.Items.Add("400,0");
                    cb_man2_fps1.Text = "0,2";
                    break;
                case 8:
                    cb_dev2_fps1.Items.Clear();
                    cb_dev2_fps1.Items.Add("---");
                    cb_dev2_fps1.Text = "---";
                    //
                    cb_man2_fps1.Items.Clear();
                    cb_man2_fps1.Items.Add("---");
                    cb_man2_fps1.Items.Add("0,2");
                    cb_man2_fps1.Text = "0,2";
                    break;
            }
        }

        private void cb_mod3_fps1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod3_fps1.SelectedIndex)
            {
                case 0:
                    cb_dev3_fps1.Items.Clear();
                    cb_dev3_fps1.Items.Add("---");
                    cb_man3_fps1.Items.Clear();
                    cb_man3_fps1.Items.Add("---");
                    cb_dev3_fps1.Text = "---";
                    cb_man3_fps1.Text = "---";
                    break;
                case 1:
                    cb_dev3_fps1.Items.Clear();
                    cb_dev3_fps1.Items.Add("---");
                    cb_dev3_fps1.Items.Add("±1,75");
                    cb_dev3_fps1.Items.Add("±2,47");
                    cb_dev3_fps1.Items.Add("±3,5");
                    cb_dev3_fps1.Items.Add("±4,2");
                    cb_dev3_fps1.Items.Add("±5,0");
                    cb_dev3_fps1.Items.Add("±7,07");
                    cb_dev3_fps1.Items.Add("±10,0");
                    cb_dev3_fps1.Items.Add("±15,8");
                    cb_dev3_fps1.Items.Add("±25,0");
                    cb_dev3_fps1.Items.Add("±35,35");
                    cb_dev3_fps1.Items.Add("±50,0");
                    cb_dev3_fps1.Items.Add("±70,0");
                    cb_dev3_fps1.Items.Add("±100,0");
                    cb_dev3_fps1.Items.Add("±150,0");
                    cb_dev3_fps1.Items.Add("±250,0");
                    cb_dev3_fps1.Items.Add("±350,0");
                    cb_dev3_fps1.Items.Add("±500,0");
                    cb_dev3_fps1.Items.Add("±700,0");
                    cb_dev3_fps1.Items.Add("±1000");
                    cb_dev3_fps1.Text = "±1,75";
                    //
                    cb_man3_fps1.Items.Clear();
                    cb_man3_fps1.Items.Add("---");
                    cb_man3_fps1.Text = "---";
                    break;
                case 2:
                case 3:
                case 4:
                    cb_dev3_fps1.Items.Clear();
                    cb_dev3_fps1.Items.Add("---");
                    cb_dev3_fps1.Items.Add("±2,0");
                    cb_dev3_fps1.Items.Add("±4,0");
                    cb_dev3_fps1.Items.Add("±6,0");
                    cb_dev3_fps1.Items.Add("±8,0");
                    cb_dev3_fps1.Items.Add("±10,0");
                    cb_dev3_fps1.Items.Add("±12,0");
                    cb_dev3_fps1.Items.Add("±14,0");
                    cb_dev3_fps1.Items.Add("±16,0");
                    cb_dev3_fps1.Items.Add("±18,0");
                    cb_dev3_fps1.Items.Add("±20,0");
                    cb_dev3_fps1.Items.Add("±22,0");
                    cb_dev3_fps1.Items.Add("±24,0");
                    cb_dev3_fps1.Text = "±2,0";
                    //
                    cb_man3_fps1.Items.Clear();
                    cb_man3_fps1.Items.Add("---");
                    cb_man3_fps1.Items.Add("62,5");
                    cb_man3_fps1.Items.Add("125");
                    cb_man3_fps1.Items.Add("250");
                    cb_man3_fps1.Items.Add("500");
                    cb_man3_fps1.Items.Add("1000");
                    cb_man3_fps1.Items.Add("2000");
                    cb_man3_fps1.Text = "62,5";
                    break;
                case 5:
                case 6:
                case 7:
                    //
                    cb_dev3_fps1.Items.Clear();
                    cb_dev3_fps1.Items.Add("---");
                    cb_dev3_fps1.Text = "---";
                    //
                    cb_man3_fps1.Items.Clear();
                    cb_man3_fps1.Items.Add("---");
                    cb_man3_fps1.Items.Add("0,2");
                    cb_man3_fps1.Items.Add("0,5");
                    cb_man3_fps1.Items.Add("1,0");
                    cb_man3_fps1.Items.Add("5,0");
                    cb_man3_fps1.Items.Add("10,0");
                    cb_man3_fps1.Items.Add("20,0");
                    cb_man3_fps1.Items.Add("40,0");
                    cb_man3_fps1.Items.Add("80,0");
                    cb_man3_fps1.Items.Add("400,0");
                    cb_man3_fps1.Text = "0,2";
                    break;
                case 8:
                    cb_dev3_fps1.Items.Clear();
                    cb_dev3_fps1.Items.Add("---");
                    cb_dev3_fps1.Text = "---";
                    //
                    cb_man3_fps1.Items.Clear();
                    cb_man3_fps1.Items.Add("---");
                    cb_man3_fps1.Items.Add("0,2");
                    cb_man3_fps1.Text = "0,2";
                    break;
            }
        }

        private void cb_mod4_fps1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod4_fps1.SelectedIndex)
            {
                case 0:
                    cb_dev4_fps1.Items.Clear();
                    cb_dev4_fps1.Items.Add("---");
                    cb_man4_fps1.Items.Clear();
                    cb_man4_fps1.Items.Add("---");
                    cb_dev4_fps1.Text = "---";
                    cb_man4_fps1.Text = "---";
                    break;
                case 1:
                    cb_dev4_fps1.Items.Clear();
                    cb_dev4_fps1.Items.Add("---");
                    cb_dev4_fps1.Items.Add("±1,75");
                    cb_dev4_fps1.Items.Add("±2,47");
                    cb_dev4_fps1.Items.Add("±3,5");
                    cb_dev4_fps1.Items.Add("±4,2");
                    cb_dev4_fps1.Items.Add("±5,0");
                    cb_dev4_fps1.Items.Add("±7,07");
                    cb_dev4_fps1.Items.Add("±10,0");
                    cb_dev4_fps1.Items.Add("±15,8");
                    cb_dev4_fps1.Items.Add("±25,0");
                    cb_dev4_fps1.Items.Add("±35,35");
                    cb_dev4_fps1.Items.Add("±50,0");
                    cb_dev4_fps1.Items.Add("±70,0");
                    cb_dev4_fps1.Items.Add("±100,0");
                    cb_dev4_fps1.Items.Add("±150,0");
                    cb_dev4_fps1.Items.Add("±250,0");
                    cb_dev4_fps1.Items.Add("±350,0");
                    cb_dev4_fps1.Items.Add("±500,0");
                    cb_dev4_fps1.Items.Add("±700,0");
                    cb_dev4_fps1.Items.Add("±1000");
                    cb_dev4_fps1.Text = "±1,75";
                    //
                    cb_man4_fps1.Items.Clear();
                    cb_man4_fps1.Items.Add("---");
                    cb_man4_fps1.Text = "---";
                    break;
                case 2:
                case 3:
                case 4:
                    cb_dev4_fps1.Items.Clear();
                    cb_dev4_fps1.Items.Add("---");
                    cb_dev4_fps1.Items.Add("±2,0");
                    cb_dev4_fps1.Items.Add("±4,0");
                    cb_dev4_fps1.Items.Add("±6,0");
                    cb_dev4_fps1.Items.Add("±8,0");
                    cb_dev4_fps1.Items.Add("±10,0");
                    cb_dev4_fps1.Items.Add("±12,0");
                    cb_dev4_fps1.Items.Add("±14,0");
                    cb_dev4_fps1.Items.Add("±16,0");
                    cb_dev4_fps1.Items.Add("±18,0");
                    cb_dev4_fps1.Items.Add("±20,0");
                    cb_dev4_fps1.Items.Add("±22,0");
                    cb_dev4_fps1.Items.Add("±24,0");
                    cb_dev4_fps1.Text = "±2,0";
                    //
                    cb_man4_fps1.Items.Clear();
                    cb_man4_fps1.Items.Add("---");
                    cb_man4_fps1.Items.Add("62,5");
                    cb_man4_fps1.Items.Add("125");
                    cb_man4_fps1.Items.Add("250");
                    cb_man4_fps1.Items.Add("500");
                    cb_man4_fps1.Items.Add("1000");
                    cb_man4_fps1.Items.Add("2000");
                    cb_man4_fps1.Text = "62,5";
                    break;
                case 5:
                case 6:
                case 7:
                    //
                    cb_dev4_fps1.Items.Clear();
                    cb_dev4_fps1.Items.Add("---");
                    cb_dev4_fps1.Text = "---";
                    //
                    cb_man4_fps1.Items.Clear();
                    cb_man4_fps1.Items.Add("---");
                    cb_man4_fps1.Items.Add("0,2");
                    cb_man4_fps1.Items.Add("0,5");
                    cb_man4_fps1.Items.Add("1,0");
                    cb_man4_fps1.Items.Add("5,0");
                    cb_man4_fps1.Items.Add("10,0");
                    cb_man4_fps1.Items.Add("20,0");
                    cb_man4_fps1.Items.Add("40,0");
                    cb_man4_fps1.Items.Add("80,0");
                    cb_man4_fps1.Items.Add("400,0");
                    cb_man4_fps1.Text = "0,2";
                    break;
                case 8:
                    cb_dev4_fps1.Items.Clear();
                    cb_dev4_fps1.Items.Add("---");
                    cb_dev4_fps1.Text = "---";
                    //
                    cb_man4_fps1.Items.Clear();
                    cb_man4_fps1.Items.Add("---");
                    cb_man4_fps1.Items.Add("0,2");
                    cb_man4_fps1.Text = "0,2";
                    break;
            }
        }
        private byte get_form_addr(int iIndex)
        {
            switch (iIndex)
            {
                case 0:
                case 2:
                    return 0x12; // 1 - 3
                case 1:
                case 3:
                    return 0x13; // 2 - 4
                case 4:
                    return 0x14; // 5
                case 5:
                    return 0x15; //6
                case 6:
                    return 0x16; // 7
                case 7:
                    return 0x17; //8
                case 8:
                    return 0x18; // 9
                default:
                    return 0x12; //1
            }
        }
        private byte get_form_addr_FPS2(int iIndex)
        {
            switch (iIndex)
            {
                case 0:
                case 1: // Адрес Литеры 1 и Литеры2
                    return 0x12;
                case 2: // Адрес Литеры 3 GPS&GLONASS
                    return 0x13;
                case 3: // Адрес Литеры 4 
                    return 0x14;
                default:
                    return 0x12;
            }
        }

        private void button_param_FPS1_Click(object sender, EventArgs e)
        {
            StringBuilder sb_hex = new StringBuilder();
            int i;
            UInt32 iFreqTmp;
            int iNum, iLen;
            uint CRC = 0;
            bool flg_Error_Param;
            byte[] arr = { 0, 0, 0, 0 };
            UInt32 ui32Tmp;
            byte[] bBufTx;
            iNum = 256;
            bBufTx = new byte[iNum];
            //------------- Адрес формирователя ---------------------------------
            bBufTx[0] = get_form_addr(comboBox_litera_FPS1.SelectedIndex);// адрес
            CRC = bBufTx[0];
            //-------------- Команда -------------------------------------------- 

            bBufTx[1] = 0x28; // команда
            CRC += bBufTx[1];

            bBufTx[2] = (byte)((comboBox_freq_num_fps1.SelectedIndex + 1) * 7 + 1); // длина пакета
            CRC += bBufTx[2];
            bBufTx[3] = (byte)(comboBox_freq_num_fps1.SelectedIndex + 1);// количество источников
            CRC += bBufTx[3];
            flg_Error_Param = true;
            switch (comboBox_litera_FPS1.SelectedIndex)
            { // в зависимости от литеры определяем диапазон
                case 0:// литера 1
                    for (i = 0; i < bBufTx[3]; i++) // проверяем диапазоны каждого источника
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 30000 && iFreqTmp < 49999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 30 - 49 МГц", "Error");
                        }
                    }
                    break;
                case 1:// литера 2
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 50000 && iFreqTmp < 89999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 50 - 89 МГц", "Error");
                        }
                    }
                    break;
                case 2:// литера 3
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 90000 && iFreqTmp < 159999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 90 - 159 МГц", "Error");
                        }
                    }
                    break;
                case 3:// литера 4
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 160000 && iFreqTmp < 289999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 160 - 289 МГц", "Error");
                        }
                    }
                    break;
                case 4:// литера 5
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 290000 && iFreqTmp < 511999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 290 - 511 МГц", "Error");
                        }
                    }
                    break;
                case 5:// литера 6
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 512000 && iFreqTmp < 859999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 512 - 859 МГц", "Error");
                        }
                    }
                    break;
                case 6:// литера 7
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 860000 && iFreqTmp < 1214999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 860 - 1214 МГц", "Error");
                        }
                    }
                    break;
                case 7:// литера 8
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 1215000 && iFreqTmp < 1999999)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 1215 - 1999 МГц", "Error");
                        }
                    }
                    break;
                case 8:// литера 9
                    for (i = 0; i < bBufTx[3]; i++)
                    {
                        iFreqTmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                        if (iFreqTmp >= 2000000 && iFreqTmp < 3000000)
                        {
                            continue;
                        }
                        else
                        {
                            flg_Error_Param = false;
                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры 2000 - 3000 МГц", "Error");
                        }
                    }
                    break;
                default:
                    break;
            }
            // перебор источников 1-4
            for (i = 0; i < bBufTx[3]; i++)
            {
                ui32Tmp = Convert.ToUInt32(tb_freq_fps1_ind[i].Text);
                arr = BitConverter.GetBytes(ui32Tmp);
                bBufTx[4 + i * 7] = arr[0];// код частоты
                CRC += bBufTx[4 + i * 7];
                bBufTx[5 + i * 7] = arr[1];// код частоты
                CRC += bBufTx[5 + i * 7];
                bBufTx[6 + i * 7] = arr[2];// код частоты
                CRC += bBufTx[6 + i * 7];
                bBufTx[7 + i * 7] = Convert.ToByte(cb_mod_fps1_ind[i].SelectedIndex);
                CRC += bBufTx[7 + i * 7];
                bBufTx[8 + i * 7] = Convert.ToByte(cb_dev_fps1_ind[i].SelectedIndex);
                CRC += bBufTx[8 + i * 7];
                bBufTx[9 + i * 7] = Convert.ToByte(cb_man_fps1_ind[i].SelectedIndex);
                CRC += bBufTx[9 + i * 7];
                bBufTx[10 + i * 7] = Convert.ToByte(cb_tim_fps1_ind[i].SelectedIndex + 1);
                CRC += bBufTx[10 + i * 7];
            }
            //
            iLen = bBufTx[2] + 3; // адрес, команда, кол. данных,
            bBufTx[iLen] = Convert.ToByte(CRC % 255);
            iLen++;
            //
            if (flg_Error_Param == true)
            {

                serialPort1.Write(bBufTx, 0, iLen); // отправляем данные на плату


                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iLen; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");

                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Action action;
            int i, iNum, iLen, localCounter, id, fr1, fr2, k1, k2, status;
            int lengthOfRow = 10;
            // int version;
            bool n_error = true;
            System.Threading.Thread.Sleep(15);
            StringBuilder sb_hex = new StringBuilder();

            //--------------------------------------------------------
            iLen = serialPort1.BytesToRead; // количество принятых байт

            if (iLen != 0)
            {
                byte[] bBufRx = new byte[iLen];
                i = 0;
                while (i < iLen)
                {
                    bBufRx[i] = Convert.ToByte(serialPort1.ReadByte());
                    sb_hex.Append(bBufRx[i].ToString("X2") + " ");
                    i++;
                }

                serialPort1.DiscardInBuffer();

                action = () =>
                {
                    if (iLen > 3)
                    {
                        //-----------------------------booot------------------------------
                        if (bBufRx[1] == 0x43)
                        {
                            //ФПС2
                            labeVer.Text = "ver:" + bBufRx[3];
                            //ФПС3
                            label29.Text = "ver:" + bBufRx[3];
                            listBoxErr_fps3.Items.Add("ver:" + bBufRx[3]);
                        }
                        else if (bBufRx[1] == 0x42)
                        {
                            //ФПС2
                            labelGetNn.Text = "=" + bBufRx[3];
                            labelGetNv.Text = "=" + bBufRx[4];
                            //ФПС3
                            listBoxErr_fps3.Items.Add("Save to EEPROM parametrs:" + bBufRx[3]);

                        }
                        else if (bBufRx[1] == 0x04)
                        {
                            int iErr = bBufRx[3] & 0x08;

                            listBoxErr.Items.Clear();
                            listBoxErr_fps3.Items.Clear();

                            if ((bBufRx[3] & 0x01) == 0x01)
                            {
                                listBoxErr.Items.Add("ошибкаCRC");
                                listBoxErr_fps3.Items.Add("ошибкаCRC");
                            }
                            if ((bBufRx[3] & 0x02) == 0x02)
                            {
                                listBoxErr.Items.Add("ошибкаCMD");
                                listBoxErr_fps3.Items.Add("ошибка DDS1");
                            }
                            if ((bBufRx[3] & 0x04) == 0x04)
                            {
                                listBoxErr.Items.Add("нет NLD1");
                                listBoxErr_fps3.Items.Add("ошибка DDS2");
                            }
                            if ((bBufRx[3] & 0x08) == 0x08)
                            {
                                listBoxErr.Items.Add("нет NLD2");
                                listBoxErr_fps3.Items.Add("нет NLD1");
                            }
                            if ((bBufRx[3] & 0x010) == 0x010)
                            {
                                listBoxErr.Items.Add("нет ответа DDS1");
                                listBoxErr_fps3.Items.Add("нет NLD1");
                            }
                            if ((bBufRx[3] & 0x020) == 0x020)
                            {
                                listBoxErr.Items.Add("нет ответа DDS2");
                                listBoxErr_fps3.Items.Add("нет ВЧ1 (L2) 2500-6000");
                            }
                            if ((bBufRx[3] & 0x040) == 0x040)
                            {
                                listBoxErr.Items.Add("нет ВЧ1");
                                listBoxErr_fps3.Items.Add("нет ВЧ2 100-500 /500-2500");
                            }
                            if ((bBufRx[3] & 0x080) == 0x080)
                            {
                                listBoxErr.Items.Add("нет ВЧ2");
                                listBoxErr_fps3.Items.Add("нет ВЧ3 (L1)");
                            }

                        }
                        else if (bBufRx[1] == 0x41)
                        {
                            //ФПС3
                            for (int j = 0; j < iLen / lengthOfRow; j++)
                            {
                                id = Convert.ToInt32(bBufRx[3 + 10 * j]);
                                fr1 = Byte2UIintConverter(bBufRx[4 + 10 * j], bBufRx[5 + 10 * j], bBufRx[6 + 10 * j]);
                                fr2 = Byte2UIintConverter(bBufRx[7 + 10 * j], bBufRx[8 + 10 * j], bBufRx[9 + 10 * j]);
                                k1 = Convert.ToInt32(bBufRx[10 + 10 * j]);
                                k2 = Convert.ToInt32(bBufRx[11 + 10 * j]);
                                status = Convert.ToInt32(bBufRx[12 + 10 * j]);
                                DrawToTable(id, fr1, fr2, k1, k2, status);
                            }
                        }

                        CallBackMy.callbackEventHandlerAq(sb_hex);

                    }
                };
                if (InvokeRequired)
                {
                    Invoke(action);
                }
                else
                {
                    action();
                }
            }
        }

        private int Byte2UIintConverter(byte b3, byte b2, byte b1)
        {
            int r = b1 << 16 | b2 << 8 | b3;
            return r;
        }

        private void DrawToTable(int id, int fr1, int fr2, int k1, int k2, int status)
        {
            try
            {
                int rowID = 0;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int test = Int32.Parse(dataGridView1.Rows[row.Index].Cells[0].Value.ToString());
                    if (test == id)
                    {
                        rowID = row.Index;
                        break;
                    }
                }

                var rowToUpdate = dataGridView1.Rows[rowID].Cells;
                rowToUpdate[1].Value = fr1;
                rowToUpdate[2].Value = fr2;
                rowToUpdate[3].Value = k1;
                rowToUpdate[4].Value = k2;
                rowToUpdate[5].Value = status;
            }
            catch (Exception exp){ }
        }

        private void button_param_FPS2_Click(object sender, EventArgs e)
        {
            StringBuilder sb_hex = new StringBuilder();
            int i, iFreqTmp;
            int iNum, iLen;
            uint CRC = 0;
            bool flg_Error_Param;
            byte[] arr = { 0, 0, 0, 0 };
            int GPS_GLONASS;
            UInt32 ui32Tmp;
            byte[] bBufTx;
            iNum = 256;
            bBufTx = new byte[iNum]; // задаем длину буфера
            flg_Error_Param = false;

            //------------- Адрес формирователя ---------------------------------
            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);// адрес
            CRC = bBufTx[0];
            //-------------- Команда -------------------------------------------- 

            if (bBufTx[0] == 0x13) // если выбрана третья литера
            {
                bBufTx[1] = 0x34;   // команда GPS & GLONASS
                CRC += bBufTx[1];   // длина пакета
                bBufTx[2] = 1;
                CRC += bBufTx[2];
                //------- Выбор диапазона -----------
                GPS_GLONASS = 0;
                if (checkBox_GPS_L1.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x01);
                }
                if (checkBox_GPS_L2.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x02);
                }
                if (checkBox_GLONASS_L1.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x04);
                }
                if (checkBox_GLONASS_L2.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x08);
                }
                bBufTx[3] = (byte)GPS_GLONASS;
                CRC += bBufTx[3];

            }
            else // если выбраны другие литеры
            {

                bBufTx[1] = 0x28; // команда
                CRC += bBufTx[1];
                // длина пакета
                bBufTx[2] = (byte)((comboBox_freq_num_fps2.SelectedIndex + 1) * 7 + 1);
                CRC += bBufTx[2];
                // количество источников
                bBufTx[3] = (byte)(comboBox_freq_num_fps2.SelectedIndex + 1);
                CRC += bBufTx[3];
                switch (comboBox_litera_FPS2.SelectedIndex)
                { // в зависимости от литеры определяем диапазон
                    case 0:// литера 1
                        for (i = 0; i < bBufTx[3]; i++) // проверяем диапазоны каждого источника
                        {
                            try
                            {
                                iFreqTmp = Convert.ToInt32(tb_freq_fps2_ind[i].Text);
                                /*                               if (iFreqTmp >= 100000 && iFreqTmp <= 500000) // 100 - 500 MHz
                                                               {
                                                                   continue;
                                                               }
                                                               else
                                                               {
                                                                   flg_Error_Param = true;
                                                                   MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры: 100 000 кГц - 500 000 кГц", "Error");
                                                                   return;
                                                               }
                               */
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Ошибка преобразования!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }



                        }
                        break;
                    case 1:// литера 2
                        for (i = 0; i < bBufTx[3]; i++)
                        {
                            iFreqTmp = Convert.ToInt32(tb_freq_fps2_ind[i].Text);
                            /*                         if (iFreqTmp >= 500000 && iFreqTmp <= 2500000)
                                                        {
                                                            continue;
                                                        }
                                                        else
                                                        {
                                                            flg_Error_Param = true;
                                                            MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры: 500 000 кГц - 2500 000 кГц", "Error");
                                                            return;
                                                        }
                                                        */
                        }
                        break;
                    case 3:// литера 4
                        for (i = 0; i < bBufTx[3]; i++)
                        {
                            iFreqTmp = Convert.ToInt32(tb_freq_fps2_ind[i].Text);
                            /*                           if (iFreqTmp >= 2500000 && iFreqTmp <= 6000000)
                                                       {
                                                           continue;
                                                       }
                                                       else
                                                       {
                                                           flg_Error_Param = true;
                                                           MessageBox.Show("Частота ИРИ № " + (i + 1).ToString() + " выходит за границы диапазона литеры: 2500 000 кГц - 6000 000 кГц", "Error");
                                                           return;
                                                       }
                           */
                        }
                        break;
                    default:
                        break;
                }
                // перебор источников 1-2
                for (i = 0; i < bBufTx[3]; i++)
                {
                    ui32Tmp = Convert.ToUInt32(tb_freq_fps2_ind[i].Text);
                    arr = BitConverter.GetBytes(ui32Tmp);
                    bBufTx[4 + i * 7] = arr[0];// код частоты
                    CRC += bBufTx[4 + i * 7];
                    bBufTx[5 + i * 7] = arr[1];// код частоты
                    CRC += bBufTx[5 + i * 7];
                    bBufTx[6 + i * 7] = arr[2];// код частоты
                    CRC += bBufTx[6 + i * 7];
                    //--------------модуляция----------------------------

                    bBufTx[7 + i * 7] = Convert.ToByte(cb_mod_fps2_ind[i].SelectedIndex);
                    CRC += bBufTx[7 + i * 7];

                    if (cb_mod_fps2_ind[i].SelectedIndex == 4) // если выбран ЛЧМ
                    {

                        // проверяем  частоту девиации 10 кГц - 127 кГц / 1МГц - 120МГц
                        try
                        {

                            iFreqTmp = Convert.ToInt32(tb_dev_fps2_ind[i].Text);
                            if (iFreqTmp >= 10 && iFreqTmp <= 120)
                            {
                                flg_Error_Param = false;

                            }
                            else if (iFreqTmp >= 1000 && iFreqTmp <= 120000)
                            {
                                iFreqTmp = iFreqTmp / 1000;
                                iFreqTmp = iFreqTmp | 0x80;

                            }
                            else
                            {
                                flg_Error_Param = true;
                                MessageBox.Show("Частота девиации выходит за границы диапазона: 10 кГц - 127 кГц / 1МГц - 120МГц", "Error");
                                return;
                            }

                            bBufTx[8 + i * 7] = Convert.ToByte(iFreqTmp);
                            CRC += bBufTx[8 + i * 7];
                            // проверяем  выбранную частоту манипуляции 1 - 200кГц
                            iFreqTmp = Convert.ToInt32(tb_man_fps2_ind[i].Text);

                            if (iFreqTmp >= 1 && iFreqTmp < 200)
                            {
                                flg_Error_Param = false;

                            }
                            else
                            {
                                flg_Error_Param = true;
                                MessageBox.Show("Частота манипуляции выходит за границы диапазона: 1 кГц - 200 кГц", "Error");
                                return;
                            }

                            bBufTx[9 + i * 7] = Convert.ToByte(iFreqTmp);
                            CRC += bBufTx[9 + i * 7];
                            bBufTx[10 + i * 7] = Convert.ToByte(cb_tim_fps2_ind[i].SelectedIndex);
                            CRC += bBufTx[10 + i * 7];

                        }

                        catch (System.Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Ошибка преобразования!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }

                    }
                    else
                    {
                        //--------------если выбрана не ЛЧМ модуляция-------------
                        bBufTx[8 + i * 7] = 0; // девиации нет
                        CRC += bBufTx[8 + i * 7];
                        bBufTx[9 + i * 7] = Convert.ToByte(cb_man_fps2_ind[i].SelectedIndex);
                        CRC += bBufTx[9 + i * 7];
                        bBufTx[10 + i * 7] = Convert.ToByte(cb_tim_fps2_ind[i].SelectedIndex);
                        CRC += bBufTx[10 + i * 7];

                    }

                }
            }

            //-------------------Отправка сообщения--------------------------------

            iLen = bBufTx[2] + 3; // длина пакета + заголовок
            bBufTx[iLen] = Convert.ToByte(CRC % 255);
            iLen++;
            //
            if (!flg_Error_Param)
            {

                serialPort1.Write(bBufTx, 0, iLen); // отправляем данные на плату


                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iLen; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");

                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void comboBox_freq_num_fps2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            switch (comboBox_freq_num_fps2.SelectedIndex)
            {
                case 0:
                    i = 0;
                    tb_freq_fps2_ind[i].Enabled = true;
                    cb_mod_fps2_ind[i].Enabled = true;
                    tb_dev_fps2_ind[i].Enabled = true;
                    cb_man_fps2_ind[i].Enabled = true;
                    tb_man_fps2_ind[i].Enabled = true;
                    cb_tim_fps2_ind[i].Enabled = true;

                    for (i = 1; i < 2; i++)
                    { // закрываем все кроме 1
                        tb_freq_fps2_ind[i].Enabled = false;
                        cb_mod_fps2_ind[i].Enabled = false;
                        tb_dev_fps2_ind[i].Enabled = false;
                        cb_man_fps2_ind[i].Enabled = false;
                        tb_man_fps2_ind[i].Enabled = false;
                        cb_tim_fps2_ind[i].Enabled = false;
                    }
                    break;
                case 1:
                    for (i = 2; i < 2; i++)
                    { // сначала все закрываем
                        tb_freq_fps2_ind[i].Enabled = false;
                        cb_mod_fps2_ind[i].Enabled = false;
                        tb_dev_fps2_ind[i].Enabled = false;
                        cb_man_fps2_ind[i].Enabled = false;
                        tb_man_fps2_ind[i].Enabled = false;
                        cb_tim_fps2_ind[i].Enabled = false;
                    }
                    for (i = 1; i < 2; i++)
                    { // потом открываем необходимые - 2
                        tb_freq_fps2_ind[i].Enabled = true;
                        cb_mod_fps2_ind[i].Enabled = true;
                        tb_dev_fps2_ind[i].Enabled = true;
                        cb_man_fps2_ind[i].Enabled = true;
                        tb_man_fps2_ind[i].Enabled = true;
                        cb_tim_fps2_ind[i].Enabled = true;
                    }
                    break;
            }
        }

        private void cb_mod1_fps2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod1_fps2.SelectedIndex)
            {
                case 0:
                    tb_dev2_fps2.Text = "----";
                    tb_man1_fps2.Text = "----";
                    cb_man1_fps2.Items.Clear();
                    cb_man1_fps2.Items.Add("----");
                    cb_man1_fps2.Text = "----";
                    break;
                case 1:
                case 2:
                case 3:
                    tb_dev1_fps2.Text = "----";
                    tb_man1_fps2.Text = "----";
                    //
                    tb_dev1_fps2.Enabled = false;
                    tb_man1_fps2.Enabled = false;
                    cb_man1_fps2.Enabled = true;

                    cb_man1_fps2.Items.Clear();
                    cb_man1_fps2.Items.Add("----");
                    cb_man1_fps2.Items.Add("0,08");
                    cb_man1_fps2.Items.Add("0,1");
                    cb_man1_fps2.Items.Add("0,133");
                    cb_man1_fps2.Items.Add("0,2");
                    cb_man1_fps2.Items.Add("0,5");
                    cb_man1_fps2.Items.Add("1,0");
                    cb_man1_fps2.Items.Add("5,0");
                    cb_man1_fps2.Items.Add("10,0");
                    cb_man1_fps2.Items.Add("20,0");
                    cb_man1_fps2.Items.Add("40,0");
                    cb_man1_fps2.Items.Add("80,0");
                    cb_man1_fps2.Items.Add("400,0");
                    cb_man1_fps2.Text = "0,2";
                    break;
                case 4:
                    //
                    tb_dev1_fps2.Text = "10";
                    tb_man1_fps2.Text = "100";
                    tb_dev1_fps2.Enabled = true;
                    tb_man1_fps2.Enabled = true;
                    cb_man1_fps2.Enabled = false;
                    //
                    cb_man1_fps2.Items.Clear();
                    cb_man1_fps2.Items.Add("----");
                    cb_man1_fps2.Text = "----";
                    break;

            }
        }

        private void cb_mod2_fps2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod2_fps2.SelectedIndex)
            {
                case 0:
                    tb_dev2_fps2.Text = "----";
                    tb_man2_fps2.Text = "----";
                    cb_man2_fps2.Items.Clear();
                    cb_man2_fps2.Items.Add("----");
                    cb_man2_fps2.Text = "----";
                    break;
                case 1:
                case 2:
                case 3:
                    tb_dev2_fps2.Text = "----";
                    tb_man2_fps2.Text = "----";
                    //
                    tb_dev2_fps2.Enabled = false;
                    tb_man2_fps2.Enabled = false;
                    cb_man2_fps2.Enabled = true;

                    cb_man2_fps2.Items.Clear();
                    cb_man2_fps2.Items.Add("----");
                    cb_man2_fps2.Items.Add("0,08");
                    cb_man2_fps2.Items.Add("0,1");
                    cb_man2_fps2.Items.Add("0,133");
                    cb_man2_fps2.Items.Add("0,2");
                    cb_man2_fps2.Items.Add("0,5");
                    cb_man2_fps2.Items.Add("1,0");
                    cb_man2_fps2.Items.Add("5,0");
                    cb_man2_fps2.Items.Add("10,0");
                    cb_man2_fps2.Items.Add("20,0");
                    cb_man2_fps2.Items.Add("40,0");
                    cb_man2_fps2.Items.Add("80,0");
                    cb_man2_fps2.Items.Add("400,0");
                    cb_man2_fps2.Text = "0,2";
                    break;
                case 4:

                    //
                    tb_dev2_fps2.Text = "10";
                    tb_man2_fps2.Text = "100";
                    tb_dev2_fps2.Enabled = true;
                    tb_man2_fps2.Enabled = true;
                    cb_man2_fps2.Enabled = false;
                    //
                    cb_man2_fps2.Items.Clear();
                    cb_man2_fps2.Text = "----";
                    break;

            }
        }

        private void comboBox_litera_FPS2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            switch (comboBox_litera_FPS2.SelectedIndex)
            {
                case 0:
                case 1:
                case 3:
                    checkBox_GPS_L1.Enabled = false;
                    checkBox_GPS_L2.Enabled = false;
                    checkBox_GLONASS_L1.Enabled = false;
                    checkBox_GLONASS_L2.Enabled = false;

                    comboBox_freq_num_fps2.Enabled = true;
                    i = 0;
                    tb_freq_fps2_ind[i].Enabled = true;
                    cb_mod_fps2_ind[i].Enabled = true;
                    tb_dev_fps2_ind[i].Enabled = true;
                    cb_man_fps2_ind[i].Enabled = true;
                    tb_man_fps2_ind[i].Enabled = true;
                    cb_tim_fps2_ind[i].Enabled = true;

                    switch (comboBox_freq_num_fps2.SelectedIndex)
                    {
                        case 0:
                            for (i = 1; i < 2; i++)
                            { // закрываем все кроме 1
                                tb_freq_fps2_ind[i].Enabled = false;
                                cb_mod_fps2_ind[i].Enabled = false;
                                tb_dev_fps2_ind[i].Enabled = false;
                                cb_man_fps2_ind[i].Enabled = false;
                                tb_man_fps2_ind[i].Enabled = false;
                                cb_tim_fps2_ind[i].Enabled = false;
                            }
                            break;
                        case 1:
                            for (i = 2; i < 2; i++)
                            { // сначала все закрываем
                                tb_freq_fps2_ind[i].Enabled = false;
                                cb_mod_fps2_ind[i].Enabled = false;
                                tb_dev_fps2_ind[i].Enabled = false;
                                cb_man_fps2_ind[i].Enabled = false;
                                tb_man_fps2_ind[i].Enabled = false;
                                cb_tim_fps2_ind[i].Enabled = false;
                            }
                            for (i = 1; i < 2; i++)
                            { // потом открываем необходимые - 2
                                tb_freq_fps2_ind[i].Enabled = true;
                                cb_mod_fps2_ind[i].Enabled = true;
                                tb_dev_fps2_ind[i].Enabled = true;
                                cb_man_fps2_ind[i].Enabled = true;
                                tb_man_fps2_ind[i].Enabled = true;
                                cb_tim_fps2_ind[i].Enabled = true;
                            }
                            break;
                    }

                    break;
                case 2:

                    checkBox_GPS_L1.Enabled = true;
                    checkBox_GPS_L2.Enabled = true;
                    checkBox_GLONASS_L1.Enabled = true;
                    checkBox_GLONASS_L2.Enabled = true;

                    comboBox_freq_num_fps2.Enabled = false;
                    for (i = 0; i < 2; i++)
                    { // закрываем все 
                        tb_freq_fps2_ind[i].Enabled = false;
                        cb_mod_fps2_ind[i].Enabled = false;
                        tb_dev_fps2_ind[i].Enabled = false;
                        cb_man_fps2_ind[i].Enabled = false;
                        tb_man_fps2_ind[i].Enabled = false;
                        cb_tim_fps2_ind[i].Enabled = false;
                    }

                    break;


            }
        }




        private void button_power_off_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x24;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];
            bBufTx[3] = 0x01;
            CRC = CRC + bBufTx[3];
            bBufTx[4] = Convert.ToByte(CRC % 255);

            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void button_power_on_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;
            UInt32 iFreqTmp;
            int iLen;
            bool flg_Error_Param;

            byte[] arr = { 0, 0, 0, 0 };
            UInt32 ui32Tmp;
            iNum = 7;
            bBufTx = new byte[iNum];
            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];

            bBufTx[1] = 0x30;
            CRC += bBufTx[1];

            bBufTx[2] = 0x03;
            CRC += bBufTx[2];
            flg_Error_Param = true;
            switch (comboBox_litera_FPS2.SelectedIndex)
            { // в зависимости от литеры определяем диапазон
                case 0:// литера 1

                    iFreqTmp = Convert.ToUInt32(tb_freq1_fps2.Text);
                    if (iFreqTmp >= 100000 && iFreqTmp <= 500000) // 150 - 550 MHz
                    {

                    }
                    else
                    {
                        flg_Error_Param = false;
                        MessageBox.Show("Частота ИРИ №  выходит за границы диапазона литеры: 100 000 кГц - 500 000 кГц", "Error");
                    }

                    break;
                case 1:// литера 2

                    iFreqTmp = Convert.ToUInt32(tb_freq1_fps2.Text);
                    if (iFreqTmp >= 550000 && iFreqTmp <= 2500000)
                    {

                    }
                    else
                    {
                        flg_Error_Param = false;
                        MessageBox.Show("Частота ИРИ выходит за границы диапазона литеры: 550 000 кГц - 2500 000 кГц", "Error");
                    }

                    break;

                default:
                    break;
            }

            ui32Tmp = Convert.ToUInt32(tb_freq1_fps2.Text);
            arr = BitConverter.GetBytes(ui32Tmp);

            bBufTx[3] = arr[0];
            CRC += bBufTx[3];
            bBufTx[4] = arr[1];
            CRC += bBufTx[4];
            bBufTx[5] = arr[2];
            CRC += bBufTx[5];

            bBufTx[6] = Convert.ToByte(CRC % 255);


            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }

        }

        private void button_status_on_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 4;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x04; // запросить состояние
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x00;
            bBufTx[3] = Convert.ToByte(CRC % 255);
            {
                serialPort1.Write(bBufTx, 0, bBufTx.Length);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iNum; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void button_power_off_FPS1_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 4;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr(comboBox_litera_FPS1.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x24;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x00;
            bBufTx[3] = Convert.ToByte(CRC % 255);
            {
                serialPort1.Write(bBufTx, 0, bBufTx.Length);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iNum; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void button_power_off_all_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x24;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];
            bBufTx[3] = 0xFF;
            CRC = CRC + bBufTx[3];
            bBufTx[4] = Convert.ToByte(CRC % 255);

            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }

        }

        private void button_Amp_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            int Nv, Nn; // верхнее и нижнее значение усиления
            int nKl, fv, fn;// инверсное значение коэффициента
            uint CRC = 0;
            int cntByteTx;

            iNum = 255;
            bBufTx = new byte[iNum];

            Nn = Convert.ToInt16(tb_ampN_fps2.Text); // нижнее значение
            if (Nn > 63 || Nn < 0)
            {
                MessageBox.Show("Коэф. Nn " + (Nn).ToString() + " выходит за границы диапазона 0-63", "Error");
                return;
            }

            Nv = Convert.ToInt16(tb_ampV_fps2.Text); // верхнее значение

            if (Nv > 63 || Nv < 0)
            {
                MessageBox.Show("Коэф. Nv " + (Nv).ToString() + " выходит за границы диапазона 0-63", "Error");
                return;
            }


            fn = 0;
            fv = 0;

            switch (comboBox_litera_FPS2.SelectedIndex)
            { // в зависимости от литеры определяем диапазон
                case 0:// литера 1
                    // 150 - 550 MHz
                    fn = 100; //кГц
                    fv = 500;
                    break;
                case 1:// литера 2
                    fn = 500; //кГц
                    fv = 2500;
                    break;
                case 2:// литера 3
                    //GNSS
                    break;
                case 3:// литера 4
                    fn = 2500; //кГц
                    fv = 5000;
                    break;

                default:
                    break;
            }


            if (Nv == Nn)
            {
                nKl = 30000;
            }
            else
            {
                nKl = (fv - fn) / (Nv - Nn); // инверсное значение коэффициента
                labelParamK.Text = "";
                labelParamK.Text = labelParamK.Text + nKl;
            }
            //------------------------
            i = 0;

            if (checkBoxService.Checked)
            {
                bBufTx[i] = 0x04; // адрес АРМА
                i++;
                bBufTx[i] = 0x05;// адрес УУ
                i++;
                bBufTx[i] = 0x47;//команда ретрансляции
                i++;
                bBufTx[i] = 0x00;//счетчик кодограмм
                i++;
                bBufTx[i] = 8;//длина пакета
                i++;
            }


            bBufTx[i] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x40; // команда
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x04; // количество данных: № литеры / КL / KM / N
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(comboBox_litera_FPS2.SelectedIndex + 1); // порядковый номер литеры
            CRC = CRC + bBufTx[i];
            i++;

            if (comboBox_litera_FPS2.SelectedIndex == 2)
            { // для GNSS литера 3

                bBufTx[i] = Convert.ToByte(Nn);
                CRC = CRC + bBufTx[i];
                i++;
                bBufTx[i] = Convert.ToByte(Nv);
                CRC = CRC + bBufTx[i];
                i++;
                bBufTx[i] = 0;

            }
            else
            {

                bBufTx[i] = Convert.ToByte(nKl & 0xFF);
                CRC = CRC + bBufTx[i];
                i++;
                nKl = nKl >> 8;
                bBufTx[i] = Convert.ToByte(nKl & 0xFF);
                CRC = CRC + bBufTx[i];
                i++;
                bBufTx[i] = Convert.ToByte(Nn);

            }

            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iNum = i;

            serialPort1.Write(bBufTx, 0, i);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void button_Test_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x41;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];
            bBufTx[3] = 0xFF;
            CRC = CRC + bBufTx[3];
            bBufTx[4] = Convert.ToByte(CRC % 255);

            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void button_Amp2_FPS2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_power2_off_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x24;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];
            bBufTx[3] = 0x02;
            CRC = CRC + bBufTx[3];
            bBufTx[4] = Convert.ToByte(CRC % 255);

            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void button_GetAmp_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];

            bBufTx[1] = 0x42; // запросить состояние усилителя
            CRC = CRC + bBufTx[1];

            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];

            bBufTx[3] = Convert.ToByte(comboBox_litera_FPS2.SelectedIndex + 1);
            CRC = CRC + bBufTx[3];

            bBufTx[4] = Convert.ToByte(CRC % 255);


            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }

        }

        private void button_Get_Ver_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 4;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(comboBox_litera_FPS2.SelectedIndex);
            CRC = bBufTx[0];

            bBufTx[1] = 0x43; // запросить версию программы
            CRC = CRC + bBufTx[1];

            bBufTx[2] = 0x00; // количество данных
            CRC = CRC + bBufTx[2];

            bBufTx[3] = Convert.ToByte(CRC % 255);


            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void labelParamK_Click(object sender, EventArgs e)
        {

        }

        private bool checkSumHex(string DataStr)
        {

            return true;

        }



        /// <summary>
        ///  отправка пакета по последовательному порту
        /// </summary>
        /// <param name="CMD"></param>
        /// <param name="address"></param>
        /// <param name="bBufInfTx"></param>
        /// <returns></returns>
        private bool sendDataMCU(byte CMD, byte address, byte[] bBufInfTx)
        {

            // все ок выходим
            return true;
        }

        private bool ReadHex32File()
        {
            return true;
        }


        private int Execute32BootCMD(DATA_COMAND cmd)
        {
            int status = 0;

            return status;
        }

        // вызывается по событию пользователя, таймера или последовательно порта
        private int Data32BootControl(SOURCE_EVENT sourceevent)
        {
            int status = 0;


            return status;
        }


        private bool ReadHexFile()
        {

            // все ок выходим
            return true;
        }

        private void bt_open_hex_Click(object sender, EventArgs e)
        {

        }
        private void timerWaitStOK_Tick(object sender, EventArgs e)
        { // ожидаем ответ по тому кто инициировал событие 

            timerWaitStOK.Enabled = false; // отключаем таймер

        }

        private void button2_Click(object sender, EventArgs e)
        {
            timerWaitStOK.Enabled = false;
        }


        private void richTextBoxOut_TextChanged(object sender, EventArgs e)
        {

            richTextBoxOut.ScrollToCaret();
        }

        private void LoadCPSettings()
        {

            // загружаем данные из файла program.xml 

            if (File.Exists("settings.xml"))
            {
                using (Stream stream = new FileStream("settings.xml", FileMode.Open))
                {

                    XmlSerializer serializer = new XmlSerializer(typeof(iniSettings));


                    TextReader reader = new StreamReader(stream);
                    // в тут же созданную копию класса iniSettings под именем iniSet


                    //     iniSettings iniSet = serializer.Deserialize(stream) as iniSettings;

                    iniSettings iniSet = (iniSettings)serializer.Deserialize(stream);

                    // и устанавливаем 
                    baudrate = iniSet.baudrate;
                    //num_ports = iniSet.num_ports;
                    portName = iniSet.portName;

                    firstFr = iniSet.firstFr;
                    secondFr = iniSet.secondFr;
                    modulation = iniSet.modulation;
                    gain = iniSet.gain;

                    gPS_L1_fps3IsChecked = iniSet.GPS_L1_fps3IsChecked;
                    gPS_L2_fps3IsChecked = iniSet.GPS_L2_fps3IsChecked;
                    gL_L1_fps3IsEChecked = iniSet.GL_L1_fps3IsChecked;
                    gL_L2_fps3IsEChecked = iniSet.GL_L2_fps3IsChecked;

                    data = iniSet.latestData;
                    cb_litera_fps3.Text = iniSet.litera;


                    richTextBoxOut.AppendText("\n Скорость порта" + baudrate + "Номер порта:" + portName);
                }


            }
            else
            {
                iniSettings iniSet = new iniSettings();
                iniSet.portName = "COM1";
                iniSet.baudrate = 115200;

                iniSet.firstFr = "150000";
                iniSet.secondFr = "250000";
                iniSet.modulation = "ЧМШ";
                iniSet.gain = "30";

                iniSet.GPS_L1_fps3IsChecked = false;
                iniSet.GPS_L2_fps3IsChecked = false;
                iniSet.GL_L1_fps3IsChecked = false;
                iniSet.GL_L2_fps3IsChecked = false;

                // выкидываем класс iniSet целиком в файл program.xml
                using (Stream writer = new FileStream("settings.xml", FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(iniSettings));
                    serializer.Serialize(writer, iniSet);
                }

            }




        }

        private void Main_form_FormClosing(object sender, FormClosingEventArgs e)
        {
            iniSettings iniSet = new iniSettings();
            if (serialPort1 != null)
            {
                iniSet.portName = serialPort1.PortName;
                iniSet.baudrate = serialPort1.BaudRate;

                iniSet.firstFr = tb_freq1_fps3.Text;
                iniSet.secondFr = tb_freq2_fps3.Text;
                iniSet.modulation = cb_mod1_fps3.Text;
                iniSet.gain = label32.Text;

                iniSet.GPS_L1_fps3IsChecked = checkBox_GPS_L1_fps3.Checked;
                iniSet.GPS_L2_fps3IsChecked = checkBox_GPS_L2_fps3.Checked;
                iniSet.GL_L1_fps3IsChecked = checkBox_GL_L1_fps3.Checked;
                iniSet.GL_L2_fps3IsChecked = checkBox_GL_L2_fps3.Checked;
                iniSet.latestData = data;
                iniSet.litera = cb_litera_fps3.Text;
            }

            // выкидываем класс iniSet целиком в файл program.xml
            using (Stream writer = new FileStream("settings.xml", FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(iniSettings));
                serializer.Serialize(writer, iniSet);
            }
        }

        private void SaveParam()
        {
            iniSettings iniSet = new iniSettings();
            if (serialPort1 != null)
            {
                iniSet.portName = serialPort1.PortName;
                iniSet.baudrate = serialPort1.BaudRate;

                iniSet.firstFr = tb_freq1_fps3.Text;
                iniSet.secondFr = tb_freq2_fps3.Text;
                iniSet.modulation = cb_mod1_fps3.Text;
                iniSet.gain = label32.Text;
                iniSet.latestData = data;
                iniSet.litera = cb_litera_fps3.Text;
            }

            // выкидываем класс iniSet целиком в файл program.xml
            using (Stream writer = new FileStream("settings.xml", FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(iniSettings));
                serializer.Serialize(writer, iniSet);
            }
        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        //TODO: refactor this copypaste

        private void cb_litera_fps3_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            switch (cb_litera_fps3.SelectedIndex)
            {
                case 0:
                case 1:
                case 3:
                    checkBox_GPS_L1_fps3.Enabled = false;
                    checkBox_GPS_L2_fps3.Enabled = false;
                    checkBox_GL_L1_fps3.Enabled = false;
                    checkBox_GL_L2_fps3.Enabled = false;

                    tb_freq1_fps3.Enabled = true;
                    tb_freq2_fps3.Enabled = true;
                    cb_mod1_fps3.Enabled = true;
                    tb_dev1_fps3.Enabled = true;
                    cb_man1_fps3.Enabled = true;
                    tb_man1_fps3.Enabled = true;
                    cb_tim1_fps3.Enabled = true;

                    break;
                case 2:

                    checkBox_GPS_L1_fps3.Enabled = true;
                    checkBox_GPS_L2_fps3.Enabled = true;
                    checkBox_GL_L1_fps3.Enabled = true;
                    checkBox_GL_L2_fps3.Enabled = true;

                    tb_freq1_fps3.Enabled = false;
                    tb_freq2_fps3.Enabled = false;
                    cb_mod1_fps3.Enabled = false;
                    tb_dev1_fps3.Enabled = false;
                    cb_man1_fps3.Enabled = false;
                    tb_man1_fps3.Enabled = false;
                    cb_tim1_fps3.Enabled = false;

                    break;


            }
        }

        private void bt_set_param_FPS3_Click(object sender, EventArgs e)
        {
            SaveParam();
            StringBuilder sb_hex = new StringBuilder();
            int i, iFreqTmp;
            int addrLit;
            int iNum, iLen;
            uint CRC = 0;
            bool flg_Error_Param;
            byte[] arr = { 0, 0, 0, 0 };
            int GPS_GLONASS;
            UInt32 ui32Tmp;
            byte[] bBufTx;
            iNum = 256;
            bBufTx = new byte[iNum]; // задаем длину буфера
            flg_Error_Param = false;

            //------------- Адрес формирователя ---------------------------------
            i = 0;
            addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;

            }

            //-----1----------
            bBufTx[i] = (byte)addrLit;
            CRC = bBufTx[i];
            i++;
            //-------------- Команда -------------------------------------------- 

            if (addrLit == 0x13) // если выбрана третья литера
            {
                bBufTx[i] = 0x34;   // команда GPS & GLONASS
                CRC += bBufTx[i];
                i++;
                bBufTx[i] = 1; // длина пакета
                CRC += bBufTx[i];
                i++;
                //------- Выбор диапазона -----------
                GPS_GLONASS = 0;
                if (checkBox_GPS_L1_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x01);
                }
                if (checkBox_GPS_L2_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x02);
                }
                if (checkBox_GL_L1_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x04);
                }
                if (checkBox_GL_L2_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x08);
                }
                bBufTx[i] = (byte)GPS_GLONASS;
                CRC += bBufTx[i];
                i++;

                if (checkBoxService.Checked)
                {
                    bBufTx[4] = (byte)(i + 1);//длина пакета + CRC
                }

            }
            else // если выбраны другие литеры
            {
                //2
                bBufTx[i] = 0x28; // команда
                CRC += bBufTx[i];
                i++;
                // длина пакета //3
                bBufTx[i] = 8;
                CRC += bBufTx[i];
                i++;
                // количество источников
                bBufTx[i] = 1; //4
                CRC += bBufTx[i];
                i++;

                iFreqTmp = Convert.ToInt32(tb_freq1_fps3.Text);
                ui32Tmp = Convert.ToUInt32(tb_freq1_fps3.Text);
                arr = BitConverter.GetBytes(ui32Tmp);
                bBufTx[i] = arr[0];// код частоты //5
                CRC += bBufTx[i];
                i++;
                bBufTx[i] = arr[1];// код частоты //6
                CRC += bBufTx[i];
                i++;
                bBufTx[i] = arr[2];// код частоты //7
                CRC += bBufTx[i];
                i++;
                //--------------модуляция----------------------------

                bBufTx[i] = Convert.ToByte(cb_mod1_fps3.SelectedIndex);
                CRC += bBufTx[i];//8
                i++;

                if (cb_mod1_fps3.SelectedIndex == 4) // если выбран ЛЧМ
                {

                    // проверяем  частоту девиации 10 кГц - 127 кГц / 1МГц - 120МГц
                    try
                    {
                        iFreqTmp = Convert.ToInt32(tb_dev1_fps3.Text);
                        if (iFreqTmp >= 10 && iFreqTmp <= 120)
                        {
                            flg_Error_Param = false;

                        }
                        else if (iFreqTmp >= 1000 && iFreqTmp <= 120000)
                        {
                            iFreqTmp = iFreqTmp / 1000;
                            iFreqTmp = iFreqTmp | 0x80;

                        }
                        else
                        {
                            flg_Error_Param = true;
                            MessageBox.Show("Частота девиации выходит за границы диапазона: 10 кГц - 127 кГц / 1МГц - 120МГц", "Error");
                            return;
                        }

                        bBufTx[i] = Convert.ToByte(iFreqTmp);//9
                        CRC += bBufTx[i];
                        i++;
                        // проверяем  выбранную частоту манипуляции 1 - 200кГц
                        iFreqTmp = Convert.ToInt32(tb_man1_fps3.Text);

                        if (iFreqTmp >= 1 && iFreqTmp < 200)
                        {
                            flg_Error_Param = false;

                        }
                        else
                        {
                            flg_Error_Param = true;
                            MessageBox.Show("Частота манипуляции выходит за границы диапазона: 1 кГц - 200 кГц", "Error");
                            return;
                        }

                        bBufTx[i] = Convert.ToByte(iFreqTmp);
                        CRC += bBufTx[i];
                        i++;
                        bBufTx[i] = Convert.ToByte(cb_tim1_fps3.SelectedIndex);
                        CRC += bBufTx[i];
                        i++;
                    }

                    catch (System.Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка преобразования!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }

                }
                else
                {
                    //--------------если выбрана не ЛЧМ модуляция-------------
                    bBufTx[i] = 0; // девиации нет
                    CRC += bBufTx[i];
                    i++;
                    bBufTx[i] = Convert.ToByte(cb_man1_fps3.SelectedIndex);
                    CRC += bBufTx[i];
                    i++;
                    bBufTx[i] = Convert.ToByte(cb_tim1_fps3.SelectedIndex);
                    CRC += bBufTx[i];
                    i++;
                    //}

                    //}
                }

                if (checkBoxService.Checked)
                {
                    bBufTx[4] = (byte)(i + 1);//длина пакета + CRC
                }

            }

            //-------------------Отправка сообщения--------------------------------


            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i; // длина пакета + заголовок
            //
            if (!flg_Error_Param)
            {

                serialPort1.Write(bBufTx, 0, iLen); // отправляем данные на плату


                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iLen; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");

                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void bt_get_status_fps3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 4;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x04; // запросить состояние
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x00;
            bBufTx[3] = Convert.ToByte(CRC % 255);
            {
                serialPort1.Write(bBufTx, 0, bBufTx.Length);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iNum; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void bt_power_off_FPS3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x24;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];
            bBufTx[3] = Convert.ToByte(cb_literaCH_fps3.SelectedIndex + 1);
            CRC = CRC + bBufTx[3];
            bBufTx[4] = Convert.ToByte(CRC % 255);

            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void cb_mod1_fps3_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod1_fps3.SelectedIndex)
            {
                case 0:
                    SaveLCHMParam();

                    tb_dev1_fps3.Text = "----";
                    tb_man1_fps3.Text = "----";
                    cb_man1_fps3.Items.Clear();
                    cb_man1_fps3.Items.Add("----");
                    cb_man1_fps3.Text = "----";
                    break;
                case 1:
                case 2:
                case 3:
                    SaveLCHMParam();

                    tb_dev1_fps3.Text = "----";
                    tb_man1_fps3.Text = "----";
                    //
                    tb_dev1_fps3.Enabled = false;
                    tb_man1_fps3.Enabled = false;
                    cb_man1_fps3.Enabled = true;

                    cb_man1_fps3.Items.Clear();
                    cb_man1_fps3.Items.Add("----");
                    cb_man1_fps3.Items.Add("0,08");
                    cb_man1_fps3.Items.Add("0,1");
                    cb_man1_fps3.Items.Add("0,133");
                    cb_man1_fps3.Items.Add("0,2");
                    cb_man1_fps3.Items.Add("0,5");
                    cb_man1_fps3.Items.Add("1,0");
                    cb_man1_fps3.Items.Add("5,0");
                    cb_man1_fps3.Items.Add("10,0");
                    cb_man1_fps3.Items.Add("20,0");
                    cb_man1_fps3.Items.Add("40,0");
                    cb_man1_fps3.Items.Add("80,0");
                    cb_man1_fps3.Items.Add("400,0");
                    cb_man1_fps3.Text = "0,2";
                    break;
                case 4:
                    //
                    tb_dev1_fps3.Text = "10";
                    tb_man1_fps3.Text = "100";
                    tb_dev1_fps3.Enabled = true;
                    tb_man1_fps3.Enabled = true;
                    cb_man1_fps3.Enabled = false;
                    //
                    cb_man1_fps3.Items.Clear();
                    cb_man1_fps3.Items.Add("----");
                    cb_man1_fps3.Text = "----";

                    LoadLCHMParam();
                    break;

            }
        }

        private void SaveLCHMParam()
        {
            curLCHMParams.Deviation = tb_dev1_fps3.Text;
            curLCHMParams.TScan = tb_man1_fps3.Text;
            curLCHMParams.TCode = cb_tim1_fps3.Text;
        }

        private void LoadLCHMParam()
        {
            tb_dev1_fps3.Text = curLCHMParams.Deviation;
            tb_man1_fps3.Text = curLCHMParams.TScan;
            cb_tim1_fps3.Text = curLCHMParams.TCode;
        }



        private void bt_Amp_fps3_Click(object sender, EventArgs e)
        {
            SaveParam();
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            int Nv, Nn; // верхнее и нижнее значение усиления
            int nKl, fv, fn;// инверсное значение коэффициента
            uint CRC = 0;
            int cntByteTx;

            iNum = 255;
            bBufTx = new byte[iNum];


            Nn = Convert.ToInt16(tb_ampNn_fps3.Text); // нижнее значение
            if (Nn > 63 || Nn < 0)
            {
                MessageBox.Show("Коэф. Nn " + (Nn).ToString() + " выходит за границы диапазона 0-63", "Error");
                return;
            }

            Nv = Convert.ToInt16(tb_ampNv_fps3.Text); // верхнее значение

            if (Nv > 63 || Nv < 0)
            {
                MessageBox.Show("Коэф. Nv " + (Nv).ToString() + " выходит за границы диапазона 0-63", "Error");
                return;
            }


            fn = Convert.ToInt32(tb_freq1_fps3.Text) / 1000;
            fv = Convert.ToInt32(tb_freq2_fps3.Text) / 1000;

            if (Nv == Nn)
            {
                nKl = 30000;
            }
            else
            {
                nKl = (fv - fn) / (Nv - Nn); // инверсное значение коэффициента
                label32.Text = "";
                label32.Text = labelParamK.Text + nKl;
            }


            i = 0;
            //---------переадресация--------
            if (checkBoxService.Checked)
            {
                bBufTx[i] = 0x04; // адрес АРМА
                i++;
                bBufTx[i] = 0x05;// адрес УУ
                i++;
                bBufTx[i] = 0x47;//команда ретрансляции
                i++;
                bBufTx[i] = 0x00;//счетчик кодограмм
                i++;
                bBufTx[i] = 8;//длина пакета
                i++;
            }


            bBufTx[i] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x40; // команда
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x04; // количество данных: № литеры / КL / KM / N
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(cb_literaK_fps3.SelectedIndex + 1); // порядковый номер литеры
            CRC = CRC + bBufTx[i];
            i++;

            if (cb_litera_fps3.SelectedIndex == 2)
            { // для GNSS литера 3

                bBufTx[i] = Convert.ToByte(Nn);
                CRC = CRC + bBufTx[i];
                i++;
                bBufTx[i] = Convert.ToByte(Nv);
                CRC = CRC + bBufTx[i];
                i++;
                bBufTx[i] = 0;

            }
            else
            {

                bBufTx[i] = Convert.ToByte(nKl & 0xFF);
                CRC = CRC + bBufTx[i];
                i++;
                nKl = nKl >> 8;
                bBufTx[i] = Convert.ToByte(nKl & 0xFF);
                CRC = CRC + bBufTx[i];
                i++;
                bBufTx[i] = Convert.ToByte(Nn);

            }

            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iNum = i;

            serialPort1.Write(bBufTx, 0, i);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void bt_GetAmp_fps3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[0];

            bBufTx[1] = 0x42; // запросить состояние усилителя
            CRC = CRC + bBufTx[1];

            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];

            bBufTx[3] = Convert.ToByte(cb_litera_fps3.SelectedIndex + 1);
            CRC = CRC + bBufTx[3];

            bBufTx[4] = Convert.ToByte(CRC % 255);


            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }


        private void bt_Get_Ver_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 4;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[0];

            bBufTx[1] = 0x43; // запросить версию программы
            CRC = CRC + bBufTx[1];

            bBufTx[2] = 0x00; // количество данных
            CRC = CRC + bBufTx[2];

            bBufTx[3] = Convert.ToByte(CRC % 255);


            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void Bt_power_off_All_FPS3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            iNum = 5;

            bBufTx = new byte[iNum];


            bBufTx[0] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[0];
            bBufTx[1] = 0x24;
            CRC = CRC + bBufTx[1];
            bBufTx[2] = 0x01;
            CRC = CRC + bBufTx[2];
            bBufTx[3] = 0xFF;
            CRC = CRC + bBufTx[3];
            bBufTx[4] = Convert.ToByte(CRC % 255);

            serialPort1.Write(bBufTx, 0, bBufTx.Length);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }


        private void FillDataGreed()
        {
            dataGridView1.ScrollBars = ScrollBars.Vertical;
            VerticalScroll.Visible = true;

            dataGridView1.DataSource = data;
            dataGridView1.RowHeadersWidth = 25;
            dataGridView1.Columns[0].Width = 30;
            dataGridView1.Columns[1].Width = 90;
            dataGridView1.Columns[2].Width = 90;
            dataGridView1.Columns[3].Width = 65;
            dataGridView1.Columns[4].Width = 65;
            dataGridView1.Columns[5].Width = 65;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {

                column.SortMode = DataGridViewColumnSortMode.Automatic;
            }

        }

        private void button_Write_Params(object sender, EventArgs e)
        {
            SaveParam();
            int dataLength;
            int bytesInDT = 9;
            byte frByte;
            byte scByte;
            byte thByte;
            byte idK1K2;
            List<int> rowIndexs = new List<int>();
            List<byte> byteList = new List<byte>();
            List<byte> mainByteList = new List<byte>();
            List<byte> subList = new List<byte>();
            StringBuilder sb_hex = new StringBuilder();
            DataGridViewCellCollection curCells = null;

            try
            {
                //------------- Адрес формирователя --------------------------------
                int i;
                int addrLit;
                int iLen;
                uint CRC = 0;


                i = 0;
                addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

                if (checkBoxService.Checked)
                {
                    mainByteList.Add(0x04); // адрес АРМА
                    mainByteList.Add(0x05);// адрес УУ
                    mainByteList.Add(0x47);//команда ретрансляции
                    mainByteList.Add(0x00);//счетчик кодограмм            
                    mainByteList.Add(8);//длина пакета
                    i = 5;
                }

                //-----1----------
                mainByteList.Add((byte)addrLit);
                CRC = mainByteList[i];
                i++;
                //-------------- Команда -------------------------------------------- 
                //2
                mainByteList.Add(0x40); // команда
                CRC += mainByteList[i];
                i++;


                var selectedCells = dataGridView1.SelectedCells;
                foreach (DataGridViewCell item in selectedCells)
                    if (!rowIndexs.Contains(item.RowIndex))
                        rowIndexs.Add(item.RowIndex);

                // длина пакета //3
                //dataLength = bytesInDT * dataGridView1.SelectedRows.Count;
                dataLength = bytesInDT * rowIndexs.Count;
                mainByteList.Add(Convert.ToByte(dataLength));
                CRC += mainByteList[i];
                i++;

                rowIndexs.Reverse();
                foreach (int rowIndex in rowIndexs)
                {
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        if (row.Index == rowIndex)
                        {
                            curCells = row.Cells;
                            for (int j = 0; j < curCells.Count; j++)
                            {
                                if (j == 0 || j % 3 == 0 || j % 4 == 0)
                                {
                                    idK1K2 = Convert.ToByte(curCells[j].Value);
                                    byteList.Add(idK1K2);
                                }
                                else
                                {
                                    FromUInt(Convert.ToUInt32(curCells[j].Value), out frByte, out scByte, out thByte);
                                    byteList.Add(frByte);
                                    byteList.Add(scByte);
                                    byteList.Add(thByte);
                                }
                            }
                        }
                    }
                    subList.AddRange(byteList);
                    byteList.Clear();
                }

                foreach (byte curByte in subList)
                {
                    CRC += curByte;
                    i++;
                }

                mainByteList.AddRange(subList);

                if (checkBoxService.Checked)
                {
                    mainByteList[4] = (byte)(i + 1);//длина пакета + CRC
                }
                //-------------------Отправка сообщения--------------------------------


                mainByteList.Add(Convert.ToByte(CRC % 255));
                i++;
                iLen = i; // длина пакета + заголовок

                //final array
                byte[] bBufDT = mainByteList.ToArray();

                serialPort1.Write(bBufDT, 0, iLen);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (int j = 0; j < iLen; j++)
                    {
                        sb_hex.Append(bBufDT[j].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
            catch (Exception exp) { }
        }

        static void FromUInt(uint number, out byte byte1, out byte byte2, out byte byte3)
        {
            byte[] arr = BitConverter.GetBytes(number);
            byte1 = arr[0];// код частоты //5
            byte2 = arr[1];// код частоты //6
            byte3 = arr[2];// код частоты //7

            //byte2 = (byte)(number >> 8);
            //byte1 = (byte)(number & 255);
        }

        private void ReverseDGVRows(DataGridView dgv)
        {
            List<DataGridViewRow> rows = new List<DataGridViewRow>();
            rows.AddRange(dgv.Rows.Cast<DataGridViewRow>());
            rows.Reverse();
            dgv.Rows.Clear();
            dgv.Rows.AddRange(rows.ToArray());
        }

        //bla-bla-bla
        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int testInt;
            try
            {
                if (int.TryParse(dataGridView1.EditingControl.Text, out testInt))
                {
                    if (e.ColumnIndex == 0)
                        idIsTaken = data.Where(x => x.Id == Convert.ToUInt32(dataGridView1.CurrentCell.EditedFormattedValue)).Any();
                }
                else
                    dataGridView1.EditingControl.Text = "0";
            }
            catch { }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            curRow = e.RowIndex;
        }



        private void dataGridView1_HeaderClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var sortedListInstance = new BindingList<Info>();
            switch (e.ColumnIndex)
            {
                case 0:
                    sortedListInstance = new BindingList<Info>(data.OrderBy(x => x.Id).ToList());
                    break;
                case 1:
                    sortedListInstance = new BindingList<Info>(data.OrderBy(x => x.Fr1).ToList());
                    break;
                case 2:
                    sortedListInstance = new BindingList<Info>(data.OrderBy(x => x.Fr2).ToList());
                    break;
                case 3:
                    sortedListInstance = new BindingList<Info>(data.OrderBy(x => x.K1).ToList());
                    break;
                case 4:
                    sortedListInstance = new BindingList<Info>(data.OrderBy(x => x.K2).ToList());
                    break;
                default:
                    sortedListInstance = new BindingList<Info>(data.OrderBy(x => x.Id).ToList());
                    break;
            }
            data = sortedListInstance;
            dataGridView1.DataSource = data;
        }

        private void button_Remove_Params(object sender, EventArgs e)
        {
            //dataGridView1.Rows.RemoveAt(curRow);
            var rowToNul = dataGridView1.Rows[curRow].Cells;
            for (int i = 1; i < rowToNul.Count; i++)
            {
                rowToNul[i].Value = "0";
            }
        }

        private void dataGridView1_IDValidating(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    var matches = data.Where(x => x.Id == Convert.ToUInt32(dataGridView1.CurrentCell.EditedFormattedValue)).Any();

                    var test1 = Convert.ToUInt32(dataGridView1.CurrentCell.EditedFormattedValue);

                    if (idIsTaken)
                    {
                        MessageBox.Show(string.Format("that id is takken: {0}", dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value));
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "255";
                    }
                }
            }
            catch { }
        }


        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string path = Path.GetFullPath(sfd.FileName);
                if (!path.EndsWith(".xml"))
                    path += ".xml";

                XmlSerializer xs = new XmlSerializer(typeof(BindingList<Info>));

                using (TextWriter tw = new StreamWriter(path))
                {
                    xs.Serialize(tw, data);
                }
            }
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {

            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK)
            {

                string path = Path.GetFullPath(ofd.FileName);
                XmlSerializer xs = new XmlSerializer(typeof(BindingList<Info>));

                using (var sr = new StreamReader(path))
                {
                    data = (BindingList<Info>)xs.Deserialize(sr);
                    dataGridView1.DataSource = data;
                }
            }

        }

        private void CleanUpButton_Click(object sender, EventArgs e)
        {
            data = new BindingList<Info>();
            dataGridView1.DataSource = data;
        }

        private void saveParamAtBooster_Click(object sender, EventArgs e)
        {
            int dataLength;
            List<byte> byteList = new List<byte>();
            List<byte> mainByteList = new List<byte>();
            StringBuilder sb_hex = new StringBuilder();

            try
            {
                //------------- Адрес формирователя --------------------------------
                int i;
                int addrLit;
                int iLen;
                uint CRC = 0;


                i = 0;
                addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

                if (checkBoxService.Checked)
                {
                    mainByteList.Add(0x04); // адрес АРМА
                    mainByteList.Add(0x05);// адрес УУ
                    mainByteList.Add(0x47);//команда ретрансляции
                    mainByteList.Add(0x00);//счетчик кодограмм            
                    mainByteList.Add(8);//длина пакета
                    i = 5;
                }

                //-----1----------
                mainByteList.Add((byte)addrLit);
                CRC = mainByteList[i];
                i++;
                //-------------- Команда -------------------------------------------- 
                //2
                mainByteList.Add(0x42); // команда
                CRC += mainByteList[i];
                i++;

                dataLength = 0;
                mainByteList.Add(Convert.ToByte(dataLength));
                CRC += mainByteList[i];
                i++;

                if (checkBoxService.Checked)
                {
                    mainByteList[4] = (byte)(i + 1);//длина пакета + CRC
                }
                //-------------------Отправка сообщения--------------------------------


                mainByteList.Add(Convert.ToByte(CRC % 255));
                i++;
                iLen = i; // длина пакета + заголовок

                //final array
                byte[] bBufDT = mainByteList.ToArray();

                serialPort1.Write(bBufDT, 0, iLen);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (int j = 0; j < iLen; j++)
                    {
                        sb_hex.Append(bBufDT[j].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
            catch { }
        }

        private void getParams_Click(object sender, EventArgs e)
        {
            int dataLength;
            List<int> rowIndexs = new List<int>();
            List<byte> byteList = new List<byte>();
            List<byte> mainByteList = new List<byte>();
            StringBuilder sb_hex = new StringBuilder();

            try
            {
                //------------- Адрес формирователя --------------------------------
                int i;
                int addrLit;
                int iLen;
                uint CRC = 0;


                i = 0;
                addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

                if (checkBoxService.Checked)
                {
                    mainByteList.Add(0x04); // адрес АРМА
                    mainByteList.Add(0x05);// адрес УУ
                    mainByteList.Add(0x47);//команда ретрансляции
                    mainByteList.Add(0x00);//счетчик кодограмм            
                    mainByteList.Add(8);//длина пакета
                    i = 5;
                }

                //-----1----------
                mainByteList.Add((byte)addrLit);
                CRC = mainByteList[i];
                i++;
                //-------------- Команда -------------------------------------------- 
                //2
                mainByteList.Add(0x41); // команда
                CRC += mainByteList[i];
                i++;

                var selectedCells = dataGridView1.SelectedCells;
                foreach (DataGridViewCell item in selectedCells)
                    if (!rowIndexs.Contains(Int32.Parse(dataGridView1.Rows[item.RowIndex].Cells[0].Value.ToString())))
                        rowIndexs.Add(Int32.Parse(dataGridView1.Rows[item.RowIndex].Cells[0].Value.ToString()));

                if (rowIndexs.Count() > 0)
                    dataLength = 1;
                else
                    dataLength = 0;

                if (rowIndexs.Count() > 1)
                    dataLength = 2;

                mainByteList.Add(Convert.ToByte(dataLength));
                CRC += mainByteList[i];
                i++;

                if (rowIndexs.Count() >= 1)
                {
                    mainByteList.Add((byte)rowIndexs.Last());
                    CRC += mainByteList[i];
                    i++;
                }
                if (rowIndexs.Count() >= 2)
                {
                    mainByteList.Add((byte)rowIndexs.First());
                    CRC += mainByteList[i];
                    i++;
                }

                if (checkBoxService.Checked)
                {
                    mainByteList[4] = (byte)(i + 1);//длина пакета + CRC
                }
                //-------------------Отправка сообщения--------------------------------

                mainByteList.Add(Convert.ToByte(CRC % 255));
                i++;
                iLen = i; // длина пакета + заголовок

                //final array
                byte[] bBufDT = mainByteList.ToArray();

                serialPort1.Write(bBufDT, 0, iLen);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (int j = 0; j < iLen; j++)
                    {
                        sb_hex.Append(bBufDT[j].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
            catch (Exception exp) { }
        }

    }
}
